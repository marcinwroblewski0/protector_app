package pl.marcinwroblewski.protector;

import junit.framework.Assert;

import org.junit.Test;

import java.util.ArrayList;

/**
 * Created by Marcin Wróblewski on 04.01.17.
 */

public class DataAnalyticsTest {

    @Test
    public void DataAnalytics_isDangerPointsLevelDangerous_240Samples_OnlyAccelerometer_ReturnsFalse() {
        System.out.println("DataAnalytics_isDangerPointsLevelDangerous_240Samples_OnlyAccelerometer_ReturnsFalse");

        ArrayList<ArrayList<Float>> accelerometerData = new ArrayList<>();

        float x = 1f;
        for(int i = 0; i < 240; i++) {
            ArrayList<Float> currentData = new ArrayList<>();
            for(int j = 0; j < 3; j++) {
                //generating sample sensor data
                currentData.add(x);
                x++;
            }
            accelerometerData.add(currentData);
        }

        System.out.println("Created " + accelerometerData.size() + " samples");

        DataAnalytics dataAnalytics = new DataAnalytics();
        int dangerPoints = dataAnalytics.accelerometerSensor(accelerometerData);
        dangerPoints *= 2; // multiply by 2 because it's a single sensor

        System.out.println("Danger points: " + dangerPoints);

        Assert.assertFalse(DataAnalytics.isDangerPointsLevelDangerous(dangerPoints, 15));
    }

    @Test
    public void DataAnalytics_isDangerPointsLevelDangerous_240Samples_ReturnsFalse() {
        System.out.println("DataAnalytics_isDangerPointsLevelDangerous_240Samples_ReturnsFalse");

        ArrayList<ArrayList<Float>> accelerometerData = new ArrayList<>();

        System.out.println("ACCELEROMETER:");
        for(int i = 0; i < 240; i++) {
            ArrayList<Float> currentData = new ArrayList<>();
            for(int j = 0; j < 3; j++) {
                //generating sample sensor data
                currentData.add(0.8f * (i*j));
            }
            accelerometerData.add(currentData);
        }

        System.out.println("Created " + accelerometerData.size() + " samples for accelerometer");

        ArrayList<ArrayList<Float>> gyroData = new ArrayList<>();

        System.out.println("GYRO:");
        for(int i = 0; i < 240; i++) {
            ArrayList<Float> currentData = new ArrayList<>();
            for(int j = 0; j < 3; j++) {
                //generating sample sensor data
                currentData.add(2f * (i*j));
            }
            gyroData.add(currentData);
        }

        System.out.println("Created " + gyroData.size() + " samples for gyro");

        DataAnalytics dataAnalytics = new DataAnalytics();
        int dangerPoints =
                dataAnalytics.accelerometerSensor(accelerometerData) +
                dataAnalytics.gravitySensor(gyroData);

        System.out.println("Danger points: " + dangerPoints);

        Assert.assertFalse(DataAnalytics.isDangerPointsLevelDangerous(dangerPoints, 15));
    }

    @Test
    public void DataAnalytics_isDangerPointsLevelDangerous_240Samples_OnlyAccelerometer_ReturnsTrue() {
        System.out.println("DataAnalytics_isDangerPointsLevelDangerous_240Samples_OnlyAccelerometer_ReturnsTrue");

        ArrayList<ArrayList<Float>> accelerometerData = new ArrayList<>();

        for(int i = 0; i < 240; i++) {
            ArrayList<Float> currentData = new ArrayList<>();
            for(int j = 0; j < 3; j++) {
                //generating sample sensor data
                currentData.add(0f);
            }
            accelerometerData.add(currentData);
        }

        System.out.println("Created " + accelerometerData.size() + " samples");

        DataAnalytics dataAnalytics = new DataAnalytics();
        int dangerPoints = dataAnalytics.accelerometerSensor(accelerometerData);
        dangerPoints *= 2; // multiply by 2 because it's a single sensor

        System.out.println("Danger points: " + dangerPoints);

        Assert.assertTrue(DataAnalytics.isDangerPointsLevelDangerous(dangerPoints, 15));
    }



























    @Test
    public void DataAnalytics_isDangerPointsLevelDangerous_60Samples_OnlyAccelerometer_ReturnsFalse() {
        System.out.println("DataAnalytics_isDangerPointsLevelDangerous_60Samples_OnlyAccelerometer_ReturnsFalse");

        ArrayList<ArrayList<Float>> accelerometerData = new ArrayList<>();

        float x = 1f;
        for(int i = 0; i < 60; i++) {
            ArrayList<Float> currentData = new ArrayList<>();
            for(int j = 0; j < 3; j++) {
                //generating sample sensor data
                currentData.add(x);
                x++;
            }
            accelerometerData.add(currentData);
        }

        System.out.println("Created " + accelerometerData.size() + " samples");

        DataAnalytics dataAnalytics = new DataAnalytics();
        int dangerPoints = dataAnalytics.accelerometerSensor(accelerometerData);
        dangerPoints *= 2; // multiply by 2 because it's a single sensor

        System.out.println("Danger points: " + dangerPoints);

        Assert.assertFalse(DataAnalytics.isDangerPointsLevelDangerous(dangerPoints, 15));
    }

    @Test
    public void DataAnalytics_isDangerPointsLevelDangerous_60Samples_ReturnsFalse() {
        System.out.println("DataAnalytics_isDangerPointsLevelDangerous_60Samples_ReturnsFalse");

        ArrayList<ArrayList<Float>> accelerometerData = new ArrayList<>();

        System.out.println("ACCELEROMETER:");
        for(int i = 0; i < 60; i++) {
            ArrayList<Float> currentData = new ArrayList<>();
            for(int j = 0; j < 3; j++) {
                //generating sample sensor data
                currentData.add(0.8f * (i*j));
            }
            accelerometerData.add(currentData);
        }

        System.out.println("Created " + accelerometerData.size() + " samples for accelerometer");

        ArrayList<ArrayList<Float>> gyroData = new ArrayList<>();

        System.out.println("GYRO:");
        for(int i = 0; i < 60; i++) {
            ArrayList<Float> currentData = new ArrayList<>();
            for(int j = 0; j < 3; j++) {
                //generating sample sensor data
                currentData.add(0.8f * (i*j));
            }
            gyroData.add(currentData);
        }

        System.out.println("Created " + gyroData.size() + " samples for gyro");

        DataAnalytics dataAnalytics = new DataAnalytics();
        int dangerPoints =
                dataAnalytics.accelerometerSensor(accelerometerData) +
                        dataAnalytics.gravitySensor(gyroData);

        System.out.println("Danger points: " + dangerPoints);

        Assert.assertFalse(DataAnalytics.isDangerPointsLevelDangerous(dangerPoints, 15));
    }

    @Test
    public void DataAnalytics_isDangerPointsLevelDangerous_60Samples_OnlyAccelerometer_ReturnsTrue() {
        System.out.println("DataAnalytics_isDangerPointsLevelDangerous_60Samples_OnlyAccelerometer_ReturnsTrue");

        ArrayList<ArrayList<Float>> accelerometerData = new ArrayList<>();

        for(int i = 0; i < 60; i++) {
            ArrayList<Float> currentData = new ArrayList<>();
            for(int j = 0; j < 3; j++) {
                //generating sample sensor data
                currentData.add(0f);
            }
            accelerometerData.add(currentData);
        }

        System.out.println("Created " + accelerometerData.size() + " samples");

        DataAnalytics dataAnalytics = new DataAnalytics();
        int dangerPoints = dataAnalytics.accelerometerSensor(accelerometerData);
        dangerPoints *= 2; // multiply by 2 because it's a single sensor

        System.out.println("Danger points: " + dangerPoints);

        Assert.assertTrue(DataAnalytics.isDangerPointsLevelDangerous(dangerPoints, 15));
    }


























    @Test
    public void DataAnalytics_isDangerPointsLevelDangerous_30Samples_OnlyAccelerometer_ReturnsFalse() {
        System.out.println("DataAnalytics_isDangerPointsLevelDangerous_30Samples_OnlyAccelerometer_ReturnsFalse");

        ArrayList<ArrayList<Float>> accelerometerData = new ArrayList<>();

        float x = 1f;
        for(int i = 0; i < 30; i++) {
            ArrayList<Float> currentData = new ArrayList<>();
            for(int j = 0; j < 3; j++) {
                //generating sample sensor data
                currentData.add(x);
                x++;
            }
            accelerometerData.add(currentData);
        }

        System.out.println("Created " + accelerometerData.size() + " samples");

        DataAnalytics dataAnalytics = new DataAnalytics();
        int dangerPoints = dataAnalytics.accelerometerSensor(accelerometerData);
        dangerPoints *= 2; // multiply by 2 because it's a single sensor

        System.out.println("Danger points: " + dangerPoints);

        Assert.assertFalse(DataAnalytics.isDangerPointsLevelDangerous(dangerPoints, 15));
    }

    @Test
    public void DataAnalytics_isDangerPointsLevelDangerous_30Samples_ReturnsFalse() {
        System.out.println("DataAnalytics_isDangerPointsLevelDangerous_30Samples_ReturnsFalse");

        ArrayList<ArrayList<Float>> accelerometerData = new ArrayList<>();

        System.out.println("ACCELEROMETER:");
        for(int i = 0; i < 30; i++) {
            ArrayList<Float> currentData = new ArrayList<>();
            for(int j = 0; j < 3; j++) {
                //generating sample sensor data
                currentData.add(0.8f * (i*j));
            }
            accelerometerData.add(currentData);
        }

        System.out.println("Created " + accelerometerData.size() + " samples for accelerometer");

        ArrayList<ArrayList<Float>> gyroData = new ArrayList<>();

        System.out.println("GYRO:");
        for(int i = 0; i < 60; i++) {
            ArrayList<Float> currentData = new ArrayList<>();
            for(int j = 0; j < 3; j++) {
                //generating sample sensor data
                currentData.add(0.8f * (i*j));
            }
            gyroData.add(currentData);
        }

        System.out.println("Created " + gyroData.size() + " samples for gyro");

        DataAnalytics dataAnalytics = new DataAnalytics();
        int dangerPoints =
                dataAnalytics.accelerometerSensor(accelerometerData) +
                        dataAnalytics.gravitySensor(gyroData);

        System.out.println("Danger points: " + dangerPoints);

        Assert.assertFalse(DataAnalytics.isDangerPointsLevelDangerous(dangerPoints, 15));
    }
    @Test
    public void DataAnalytics_isDangerPointsLevelDangerous_30Samples_OnlyAccelerometer_ReturnsTrue() {
        System.out.println("DataAnalytics_isDangerPointsLevelDangerous_30Samples_OnlyAccelerometer_ReturnsTrue");

        ArrayList<ArrayList<Float>> accelerometerData = new ArrayList<>();

        for(int i = 0; i < 30; i++) {
            ArrayList<Float> currentData = new ArrayList<>();
            for(int j = 0; j < 3; j++) {
                //generating sample sensor data
                currentData.add(0f);
            }
            accelerometerData.add(currentData);
        }

        System.out.println("Created " + accelerometerData.size() + " samples");

        DataAnalytics dataAnalytics = new DataAnalytics();
        int dangerPoints = dataAnalytics.accelerometerSensor(accelerometerData);
        dangerPoints *= 2; // multiply by 2 because it's a single sensor

        System.out.println("Danger points: " + dangerPoints);

        Assert.assertTrue(DataAnalytics.isDangerPointsLevelDangerous(dangerPoints, 15));
    }

}
