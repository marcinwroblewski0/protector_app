package pl.marcinwroblewski.protector;

import org.junit.Assert;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
/**
 * Created by Admin on 29.12.2016.
 */


public class IsTheSameTest {

    @Test
    public void DataAnalitics_isTheSame_ReturnsTrue() {
        Assert.assertThat(DataAnalytics.isTheSame(new float[]{1.0f, 2.0f}, new float[]{1f, 2f}), is(true));
    }

    @Test
    public void DataAnalitics_isTheSame_ReturnsFalse() {
        Assert.assertThat(DataAnalytics.isTheSame(new float[]{1.0f, 2.5f}, new float[]{1.0f, 3f}), is(false));
    }
}
