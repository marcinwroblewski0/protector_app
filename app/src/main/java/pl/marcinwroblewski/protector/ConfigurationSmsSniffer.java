package pl.marcinwroblewski.protector;

import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import me.everything.providers.android.telephony.Sms;
import me.everything.providers.android.telephony.TelephonyProvider;
import pl.marcinwroblewski.protector.guardian.Guardian;
import pl.marcinwroblewski.protector.pupil.addingGuardian.AddingNewGuardianState;
import pl.marcinwroblewski.protector.pupil.addingGuardian.GuardianAddingOperationsList;

/**
 * Created by Marcin Wróblewski on 28.01.2017.
 */

public class ConfigurationSmsSniffer {

    private Context context;
    private TelephonyProvider provider;

    public ConfigurationSmsSniffer(Context context) {
        this.context = context;
        provider = new TelephonyProvider(context);
    }

    public ArrayList<Sms> getAllConfigurationMessages() {
        List<Sms> allSmses = provider.getSms(TelephonyProvider.Filter.ALL).getList();
        ArrayList<Sms> configurationSmses = new ArrayList<>();

        for (Sms sms : allSmses) {
            if(sms.body.contains(SmsCommuniation.ENTRY_FORMULA)) {
                configurationSmses.add(sms);

                Log.d("Sms sniffer", sms.body);
            }
        }

        return configurationSmses;
    }

    public ArrayList<Sms> extractConversation(String guardianPhoneNumber) {
        ArrayList<Sms> conversationSmses = new ArrayList<>();

        for (Sms sms : getAllConfigurationMessages()) {
            if(sms.address.equals(guardianPhoneNumber)) {
                conversationSmses.add(sms);

                Log.d("Sms sniffer", "message for " + guardianPhoneNumber + ": " + sms.body);
            }
        }
        return conversationSmses;
    }

    public ArrayList<Guardian> findRequestingGuardians() {
        ArrayList<Guardian> askingGuardians = new ArrayList<>();

        for (Sms sms : getAllConfigurationMessages()) {
            try {
            int dataStartingAt = sms.body.indexOf(':') + 1;
            String informations = sms.body.substring(dataStartingAt);
                JSONObject jsonMessage = new JSONObject(informations);

                if(jsonMessage.getInt(SmsCommuniation.COMMUNICATION_CODE) == AddingNewGuardianState.ADDING_REQUEST) {
                    askingGuardians.add(Guardian.fromJSONObject(jsonMessage.getJSONObject(SmsCommuniation.ID)));
                    Log.d("Sms sniffer", sms.address + " asked for adding as guardian:\n" + sms.body);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return askingGuardians;
    }

    public void restoreRequestStatus(ArrayList<Sms> conversation, Guardian askingGuardian) {
        for (Sms sms : conversation) {
            try {
                int dataStartingAt = sms.body.indexOf(':') + 1;
                String informations = sms.body.substring(dataStartingAt);

                JSONObject jsonMessage = new JSONObject(informations);
                int communicationCode = jsonMessage.getInt(SmsCommuniation.COMMUNICATION_CODE);

                AddingNewGuardianState state = new AddingNewGuardianState(
                        askingGuardian, communicationCode
                );

                switch (communicationCode) {
                    case AddingNewGuardianState.ADDING_DONE:
                        if(GuardianAddingOperationsList.addNewOperation(state, context)
                                .getStatus().equals(OperationResult.STATUS_ERROR)) {
                            GuardianAddingOperationsList.modifyOperation(
                                    state.getRequestingGuardian(),
                                    state.getOperationState(),
                                    context);
                        }
                        Log.d("Restore", "Added guardian:\n" + state.getRequestingGuardian().toJSONObject().toString(2));
                        break;
                    case AddingNewGuardianState.REJECT_REQUEST:
                    case AddingNewGuardianState.REJECT_PASSWORD:
                        if(GuardianAddingOperationsList.addNewOperation(state, context)
                                .getStatus().equals(OperationResult.STATUS_ERROR)) {
                            GuardianAddingOperationsList.modifyOperation(
                                    state.getRequestingGuardian(),
                                    state.getOperationState(),
                                    context);
                        }
                        Log.d("Restore", "Rejected guardian:\n" + state.getRequestingGuardian().toJSONObject().toString(2));
                        break;
                    case AddingNewGuardianState.WAITING_FOR_PASSWORD_FROM_GUARDIAN:
                        if(GuardianAddingOperationsList.addNewOperation(state, context)
                                .getStatus().equals(OperationResult.STATUS_ERROR)) {
                            GuardianAddingOperationsList.modifyOperation(
                                    state.getRequestingGuardian(),
                                    state.getOperationState(),
                                    context);
                        }
                        Log.d("Restore", "Waiting for password from:\n" + state.getRequestingGuardian().toJSONObject().toString(2));
                        break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}
