package pl.marcinwroblewski.protector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.provider.Telephony;
import android.telephony.SmsManager;
import android.telephony.SmsMessage;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import pl.marcinwroblewski.protector.guardian.Guardian;
import pl.marcinwroblewski.protector.guardian.addingPupil.AddingNewPupilState;
import pl.marcinwroblewski.protector.guardian.requests.SendingRequests;
import pl.marcinwroblewski.protector.pupil.Pupil;
import pl.marcinwroblewski.protector.pupil.addingGuardian.AddingNewGuardianState;


public class NewSmsReceiver extends BroadcastReceiver {

    private static final String LOG = "IncomingSMS";
    private Context context;

	// Get the object of SmsManager
	final SmsManager sms = SmsManager.getDefault();
	
	public void onReceive(Context context, Intent intent) {
        Log.d("Message", "SMS received!");
        this.context = context;

        if (Build.VERSION.SDK_INT <= 19) { //below KITKAT

            final Bundle bundle = intent.getExtras();
            Object pdus[] = (Object[]) bundle.get("pdus");

			if (bundle != null) {
				final Object[] pdusObj = (Object[]) bundle.get("pdus");

                SmsMessage[] smsMessages = new SmsMessage[pdusObj.length];
				for (int i = 0; i < pdusObj.length; i++) {
					SmsMessage smsMessage = SmsMessage.createFromPdu((byte[]) pdus[i]);
                    smsMessages[i] = smsMessage;
				}

                translateMessage(smsMessages);
		  	}
        } else {
            SmsMessage[] messages = Telephony.Sms.Intents.getMessagesFromIntent(intent);

            Log.d("Messages", "Incoming " + messages.length);

            translateMessage(messages);

        }
	}

	private void translateMessage(SmsMessage... smsMessages) {

        String phoneNumber = smsMessages[0].getDisplayOriginatingAddress();
        String senderNum = phoneNumber;

        String message = new String();
        for (SmsMessage smsMessage : smsMessages) {
            message += smsMessage.getDisplayMessageBody();
            Log.d("translate part", smsMessage.getDisplayMessageBody());
        }

        Log.d("translate all", message);

        if(!message.contains(SmsCommuniation.ENTRY_FORMULA)) {
            Log.d(LOG, "Incoming sms wasn't valid protector communication sms");
            return;
        }

        try {
            int dataStartingAt = message.indexOf(':') + 1;
            String informations = message.substring(dataStartingAt);

            JSONObject jsonMessage = new JSONObject(informations);
            String messageContext = jsonMessage.getString(SmsCommuniation.CONTEXT);

            Log.d("receivedSMS", "JSONObject created: " + jsonMessage.toString(4));

            if(messageContext.equals(AddingNewGuardianState.CONTEXT)) {
                //info for guardian
                int communicationCode = jsonMessage.getInt(SmsCommuniation.COMMUNICATION_CODE);
                Pupil pupil = Pupil.fromJSONObject(jsonMessage.getJSONObject(SmsCommuniation.ID));

                //assume that pupil responded for request asking
                if(communicationCode == AddingNewPupilState.PUPIL_ACCEPTED ||
                        communicationCode == AddingNewPupilState.PUPIL_REJECTED) {
                    SmsCommuniation.ForGuardian.pupilRespondedRequest(pupil, communicationCode, context);
                }

                if(communicationCode == AddingNewPupilState.PUPIL_ACCEPTED_PASSWORD ||
                        communicationCode == AddingNewPupilState.PUPIL_REJECTED_PASSWORD) {
                    Log.d("Sms", SmsCommuniation.ForGuardian.pupilConfirmedAddingGuardian(pupil, communicationCode, context).getMessage() + ", code: " + communicationCode);
                }

                if(communicationCode == AddingNewPupilState.ADDING_DONE) {
                    SmsCommuniation.ForGuardian.pupilAddingDone(pupil, context);
                }

            } else if (messageContext.equals(AddingNewPupilState.CONTEXT)) {
                //info for pupil
                int communicationCode = jsonMessage.getInt(SmsCommuniation.COMMUNICATION_CODE);
                Guardian guardian = Guardian.fromJSONObject(jsonMessage.getJSONObject(SmsCommuniation.ID));

                if(communicationCode == AddingNewGuardianState.ADDING_REQUEST) {
                    SmsCommuniation.ForPupil.addingGuardianRequest(guardian, context);
                }

                if(communicationCode == AddingNewGuardianState.PASSWORD_RECEIVED) {
                    String password = jsonMessage.getJSONObject(SmsCommuniation.DATA).getString("password");
                    SmsCommuniation.ForPupil.receivePasswordFromGuardian(guardian, password, context);
                }

                if(communicationCode == SendingRequests.LOCATION_REQUEST_CODE) {
                    String password = jsonMessage.getJSONObject(SmsCommuniation.DATA).getString("password");
                    SmsCommuniation.ForPupil.handleLocationRequest(guardian, password, context);
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("SmsReceiver", "senderNum: "+ senderNum + "; message: " + message);
    }
	
}