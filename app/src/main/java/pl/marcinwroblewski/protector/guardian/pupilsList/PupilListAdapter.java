package pl.marcinwroblewski.protector.guardian.pupilsList;

import android.content.Context;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageButton;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import pl.marcinwroblewski.protector.ProtectorApplication;
import pl.marcinwroblewski.protector.R;
import pl.marcinwroblewski.protector.guardian.addingPupil.AddingNewPupilState;
import pl.marcinwroblewski.protector.guardian.addingPupil.PupilAddingOperationsList;
import pl.marcinwroblewski.protector.guardian.requests.RequestingPasswordPanel;
import pl.marcinwroblewski.protector.pupil.Pupil;

import static com.geniusforapp.fancydialog.FancyAlertDialog.TAG;

/**
 * Created by Marcin Wróblewski on 18.01.2017.
 */

public class PupilListAdapter extends RecyclerView.Adapter<PupilListAdapter.MyViewHolder> {

    private AddingNewPupilState[] states;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView guardianName, guardianPhoneNumber;
        public ImageView imageState;
        public AppCompatImageButton deleteButton;
        public AppCompatButton requestLocation;

        public MyViewHolder(View view) {
            super(view);
            guardianName = view.findViewById(R.id.guardian_name);
            guardianPhoneNumber = view.findViewById(R.id.guardian_phone_number);
            imageState = view.findViewById(R.id.state_image);
            deleteButton = view.findViewById(R.id.delete_button);
            requestLocation = view.findViewById(R.id.request_location);
        }
    }

    public PupilListAdapter(AddingNewPupilState[] states, Context context) {
        this.states = states;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.pupil_list_element, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.guardianName.setText(states[position].getAskedPupil().getFirstName() + " " +
                    states[position].getAskedPupil().getLastName());
            holder.guardianPhoneNumber.setText(states[position].getAskedPupil().getPhoneNumber());
            holder.requestLocation.setVisibility(View.GONE);

            switch (states[position].getOperationState()) {
                case AddingNewPupilState.ADDING_DONE:
                    holder.imageState.setImageResource(R.drawable.ic_added);
                    setupRequestLocationButton(states[position].getAskedPupil(), holder.requestLocation);
                    break;
                case AddingNewPupilState.ASKED_PUPIL_FOR_ADDING:
                    holder.imageState.setImageResource(R.drawable.ic_pending);
                    prepareDeleteButton(holder, states[position]);
                    break;
                case AddingNewPupilState.WAITING_FOR_PUPIL_RESPONSE_ABOUT_ADDING:
                    holder.imageState.setImageResource(R.drawable.ic_pending);
                    break;
                case AddingNewPupilState.PASSWORD_SENT_TO_PUPIL:
                    holder.imageState.setImageResource(R.drawable.ic_pending);
                    break;
                case AddingNewPupilState.ADDING_REJECTED:
                    holder.imageState.setImageResource(R.drawable.ic_rejected);
                    break;
            }
        }

    private void setupRequestLocationButton(final Pupil pupil, View button) {
        button.setVisibility(View.VISIBLE);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ((ProtectorApplication) context.getApplicationContext()).setPupilThatBeingRequested(pupil);
                BottomSheetDialogFragment dialog = new RequestingPasswordPanel();

                try {
                    FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();
                    dialog.show(fragmentManager, dialog.getTag());
                } catch (ClassCastException e) {
                    Log.e(TAG, "Can't get fragment manager");
                }
            }
        });

    }


    @Override
    public int getItemCount() {
        return states.length;
    }

    private void prepareDeleteButton(MyViewHolder holder, final AddingNewPupilState state) {
        holder.deleteButton.setVisibility(View.VISIBLE);
        holder.deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("Delete", PupilAddingOperationsList.removeOperation(state, context).toString());
            }
        });
    }


}
