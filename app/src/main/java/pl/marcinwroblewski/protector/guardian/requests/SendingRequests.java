package pl.marcinwroblewski.protector.guardian.requests;

import android.content.Context;

import pl.marcinwroblewski.protector.SmsCommuniation;
import pl.marcinwroblewski.protector.guardian.addingPupil.PasswordsForPupils;
import pl.marcinwroblewski.protector.pupil.Pupil;

/**
 * Created by Marcin Wróblewski on 02.07.2017.
 */

public class SendingRequests {

    public static final int LOCATION_REQUEST_CODE = 2001;
    private Pupil pupil;
    private String password;
    private int requestCode;
    private Context context;

    public SendingRequests(Pupil pupil, String password, int requestCode, Context context) {
        this.pupil = pupil;
        this.password = password;
        this.requestCode = requestCode;
        this.context = context;
    }

    public void perform() {
        switch (requestCode) {
            case LOCATION_REQUEST_CODE:
                sendLocationRequest();
                break;
        }
    }

    private void sendLocationRequest() {

        if(!PasswordsForPupils.comparePasswords(pupil, password, context)) {
            throw new SecurityException("Passwords does not match");
        }

        SmsCommuniation.ForGuardian.sendLocationRequest(pupil, context);

    }
}
