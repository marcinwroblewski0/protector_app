package pl.marcinwroblewski.protector.guardian.pupilsList;


import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pl.marcinwroblewski.protector.R;
import pl.marcinwroblewski.protector.guardian.addingPupil.AddingNewPupilState;
import pl.marcinwroblewski.protector.guardian.addingPupil.PupilAddingOperationsList;

public class PupilListFragment extends Fragment {

    private RecyclerView recyclerView;
    private PupilListAdapter adapter;
    private AddingNewPupilState[] states;
    private Handler handler;
    private Runnable updateTask = new Runnable() {
        @Override
        public void run() {
            if(getContext() != null) {
                states = PupilAddingOperationsList.getAllOperationsState(getContext());
                recyclerView.setAdapter(new PupilListAdapter(states, getContext()));
            }
            handler.postDelayed(this, 5000);
        }
    };

    public PupilListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
        states = PupilAddingOperationsList.getAllOperationsState(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView =  inflater.inflate(R.layout.fragment_pupil_list, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.pupils_recycler_view);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(new PupilListAdapter(states, getContext()));

        View addPupilButton = rootView.findViewById(R.id.add_pupil);
        addPupilButton.setClickable(true);
        addPupilButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new AddPupilDialogFragment().show(getFragmentManager(), "Add pupil");
            }
        });

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        handler.post(updateTask);
    }
}
