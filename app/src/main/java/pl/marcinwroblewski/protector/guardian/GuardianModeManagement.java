package pl.marcinwroblewski.protector.guardian;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Admin on 14.01.2017.
 */

public class GuardianModeManagement {

    public static final String GUARDIAN_MODE_ENABLED = "guardianModeEnabled";
    public static final String GUARDIAN_FIRST_NAME = "guardianFirstName";
    public static final String GUARDIAN_LAST_NAME = "guardianLastName";

    public static void enableGuardianMode(Guardian guardianInfo, Context context) {

        //TODO Billing API

        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(GUARDIAN_MODE_ENABLED, true);
        editor.putString(GUARDIAN_FIRST_NAME, guardianInfo.getFirstName());
        editor.putString(GUARDIAN_LAST_NAME, guardianInfo.getLastName());
        editor.apply();
    }


}
