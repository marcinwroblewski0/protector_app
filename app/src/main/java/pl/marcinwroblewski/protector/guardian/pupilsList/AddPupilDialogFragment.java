package pl.marcinwroblewski.protector.guardian.pupilsList;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.DialogFragment;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.Toast;

import pl.marcinwroblewski.protector.R;
import pl.marcinwroblewski.protector.SmsCommuniation;
import pl.marcinwroblewski.protector.guardian.addingPupil.PasswordsForPupils;
import pl.marcinwroblewski.protector.pupil.Pupil;

/**
 * Created by Marcin Wróblewski on 22.01.2017.
 */

public class AddPupilDialogFragment extends DialogFragment {

    private View mainView;
    private TextInputLayout phoneNumberTI, firstNameTI, lastNameTI, passwordTI;
    private EditText phoneNumberET, firstNameET, lastNameET, passwordET;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.add_pupil_dialog_fragment, container);
        getDialog().setCancelable(false);
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        setupForm();
        setupButtons();
        return mainView;
    }

    private void setupForm() {
        phoneNumberTI = (TextInputLayout) mainView.findViewById(R.id.pupil_phone_number);
        firstNameTI = (TextInputLayout) mainView.findViewById(R.id.pupil_first_name);
        lastNameTI = (TextInputLayout) mainView.findViewById(R.id.pupil_last_name);
        passwordTI = (TextInputLayout) mainView.findViewById(R.id.pupil_password);

        phoneNumberET = (EditText) mainView.findViewById(R.id.et_pupil_phone_number);
        firstNameET = (EditText) mainView.findViewById(R.id.et_pupil_first_name);
        lastNameET = (EditText) mainView.findViewById(R.id.et_pupil_last_name);
        passwordET = (EditText) mainView.findViewById(R.id.et_pupil_password);
    }

    private void setupButtons() {
        View cancelButton = mainView.findViewById(R.id.button_cancel);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });

        View askButton = mainView.findViewById(R.id.button_ask);
        askButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(!validatePhoneNumber(phoneNumberET.getText().toString())) {
                    return;
                }

                Pupil askedPupil = new Pupil(
                        phoneNumberET.getText().toString(),
                        firstNameET.getText().toString(),
                        lastNameET.getText().toString());

                PasswordsForPupils.assign(askedPupil, passwordET.getText().toString(), getContext());
                SmsCommuniation.ForGuardian.askPupilForAdding(askedPupil, getContext());

                Toast.makeText(getContext(), "Pupil adding process done. Waiting for the response...", Toast.LENGTH_LONG).show();
                dismiss();
            }
        });
    }

    private boolean validatePhoneNumber(String phoneNumber) {
        if(phoneNumber.length() <= 4) {
            phoneNumberTI.setError("Phone number is too short");
        }

        if(!Patterns.PHONE.matcher(phoneNumber).matches()) {
            phoneNumberTI.setError("It's not valid phone number");
        }

        return true;
    }
}
