package pl.marcinwroblewski.protector.guardian.addingPupil;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import pl.marcinwroblewski.protector.OperationResult;
import pl.marcinwroblewski.protector.pupil.Pupil;

/**
 * Created by Marcin Wróblewski on 18.01.2017.
 */

public class PasswordsForPupils {

    private static final String ALL_PASSWORDS = "pupilPasswords";
    private static final String PUPIL = "pupil";
    private static final String PASSWORD = "password";

    public static OperationResult assign(Pupil pupil, String password, Context context) {

        Set<String> allPasswords = getAllPasswords(context);

        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(),
                Context.MODE_PRIVATE
        );
        SharedPreferences.Editor editor = preferences.edit();

        try {
            JSONObject guardiansPasswordObject = new JSONObject();
            guardiansPasswordObject.put(PUPIL, pupil.toJSONObject());
            guardiansPasswordObject.put(PASSWORD, password);
            allPasswords.add(guardiansPasswordObject.toString());
            editor.putStringSet(ALL_PASSWORDS, allPasswords);
            editor.apply();
            return new OperationResult(
                    OperationResult.STATUS_SUCCESS,
                    "Password for guardian added successfully"
            );
        } catch (JSONException e) {
            e.printStackTrace();
            return new OperationResult(
                    OperationResult.STATUS_ERROR,
                    "Cannot add password for guardian: \n" + e.getMessage()
            );
        }
    }

    public static boolean comparePasswords(Pupil pupil, String password, Context context) {

        String passwordForPupil = getPasswordFor(pupil, context);
        if(passwordForPupil == null) {
            return false;
        }

        if(passwordForPupil.equals(password)) return true;
        else return false;
    }

    public static String getPasswordFor(Pupil pupil, Context context) {
        Set<String> allPasswords = getAllPasswords(context);

        for (String pupilsPasswordString : allPasswords) {
            try {
                JSONObject pupilsPasswordObject = new JSONObject(pupilsPasswordString);
                Pupil currentPupil = Pupil.fromJSONObject(pupilsPasswordObject.getJSONObject(PUPIL));

                if(Pupil.areTheSame(currentPupil, pupil)) {
                    return pupilsPasswordObject.getString(PASSWORD);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private static Set<String> getAllPasswords(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(),
                Context.MODE_PRIVATE
        );

        return preferences.getStringSet(ALL_PASSWORDS, new HashSet<String>());
    }

}
