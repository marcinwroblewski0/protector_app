package pl.marcinwroblewski.protector.guardian.addingPupil;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import pl.marcinwroblewski.protector.OperationResult;
import pl.marcinwroblewski.protector.pupil.Pupil;

/**
 * Created by Marcin Wróblewski on 16.01.2017.
 */

public class PupilAddingOperationsList {

    public static final String ALL_PUPIL_ADDING_OPERATIONS = "allPupilAddingOperations";

    public static OperationResult addNewOperation(AddingNewPupilState state, Context context) {

        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Set<String> allOperations = getAllOperationsStringSet(context);

        if(isAlreadyInList(state, context)) return new OperationResult(OperationResult.STATUS_ERROR,
                "State is already in list");

        try {
            allOperations.add(state.toJSONObject().toString());
            editor.putStringSet(ALL_PUPIL_ADDING_OPERATIONS, allOperations);
            editor.apply();
            return new OperationResult(
                    OperationResult.STATUS_SUCCESS,
                    "Adding operation went successfully");
        } catch (JSONException e) {
            return new OperationResult(
                    OperationResult.STATUS_ERROR,
                    "Cannot add new operation to list: \n"
                            + e.getMessage());

        }
    }

    public static OperationResult removeOperation(AddingNewPupilState state, Context context) {

        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Set<String> allOperations = getAllOperationsStringSet(context);

        if(!isAlreadyInList(state, context)) return new OperationResult(OperationResult.STATUS_ERROR,
                "Cannot remove operation that's not in list");

        try {
            allOperations.remove(state.toJSONObject().toString());

            editor.putStringSet(ALL_PUPIL_ADDING_OPERATIONS, allOperations);
            editor.apply();
            return new OperationResult(
                    OperationResult.STATUS_SUCCESS,
                    "Removing operation went successfully");
        } catch (JSONException e) {
            return new OperationResult(
                    OperationResult.STATUS_ERROR,
                    "Cannot remove operation to list: \n"
                            + e.getMessage());

        }
    }

    public static boolean isAlreadyInList(AddingNewPupilState state, Context context) {
        Set<String> allOperations = getAllOperationsStringSet(context);
        for (String operationString : allOperations) {
            try {
                JSONObject operationObject = new JSONObject(operationString);
                Pupil operationObjectPupil = Pupil.fromJSONObject(
                        operationObject.getJSONObject(AddingNewPupilState.ASKED_PUPIL));

                if (Pupil.areTheSame(operationObjectPupil, state.getAskedPupil()))
                    return true;

                if(operationObjectPupil.getPhoneNumber().equals(state.getAskedPupil().getPhoneNumber())) {
                    return true;
                }

            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }

        return false;
    }

    public static Set<String> getAllOperationsStringSet(Context context) {

        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);

        //using object strait from .getStringSet is forbidden in the docs
        Set<String> sharedPrefsSet = preferences.getStringSet(ALL_PUPIL_ADDING_OPERATIONS,
                new HashSet<String>());
        HashSet<String> operations = new HashSet<String>();
        operations.addAll(sharedPrefsSet);

        return operations;
    }

    public static AddingNewPupilState[] getAllOperationsState(Context context) {

        Set<String> allOperationsSet = getAllOperationsStringSet(context);
        AddingNewPupilState[] newPupilStates = new AddingNewPupilState[allOperationsSet.size()];

        int i = 0;
        for (String operation : allOperationsSet) {
            try {
                newPupilStates[i] = AddingNewPupilState.fromJSONObject(
                        new JSONObject(operation));
                i++;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return newPupilStates;
    }

    public static AddingNewPupilState getOperationFor(Pupil pupil, Context context) {

        for (AddingNewPupilState stateInList : getAllOperationsState(context)) {
            if (Pupil.areTheSame(stateInList.getAskedPupil(), pupil))
                return stateInList;
        }

        return null;
    }

    public static OperationResult modifyOperation(Pupil askedPupil, int newState, Context context) {
        AddingNewPupilState stateToModify = getOperationFor(askedPupil, context);

        if(stateToModify == null)
            return new OperationResult(OperationResult.STATUS_ERROR,
                    "There's no operation for provided pupil");

        AddingNewPupilState modifiedState = new AddingNewPupilState(askedPupil, newState);

        removeOperation(stateToModify, context);
        addNewOperation(modifiedState, context);

        return new OperationResult(OperationResult.STATUS_SUCCESS,
                "Operation modified successfully");
    }

}
