package pl.marcinwroblewski.protector.guardian.requests;

import android.app.Dialog;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.design.widget.CoordinatorLayout;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import pl.marcinwroblewski.protector.ProtectorApplication;
import pl.marcinwroblewski.protector.R;
import pl.marcinwroblewski.protector.SmsCommuniation;
import pl.marcinwroblewski.protector.guardian.addingPupil.PasswordsForPupils;
import pl.marcinwroblewski.protector.pupil.Pupil;

/**
 * Created by Marcin Wróblewski on 02.07.2017.
 */

public class RequestingPasswordPanel extends BottomSheetDialogFragment {

    private Pupil pupil;
    private Button confirmButton;
    private EditText passwordInput;
    private View view;

    private BottomSheetBehavior.BottomSheetCallback mBottomSheetBehaviorCallback = new BottomSheetBehavior.BottomSheetCallback() {

        @Override
        public void onStateChanged(@NonNull View bottomSheet, int newState) {
            if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }

        }

        @Override
        public void onSlide(@NonNull View bottomSheet, float slideOffset) {
        }
    };

    @Override
    public void setupDialog(Dialog dialog, int style) {
        super.setupDialog(dialog, style);
        view = View.inflate(getContext(), R.layout.password_ask_bottom_sheet, null);
        dialog.setContentView(view);

        CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) ((View) view.getParent()).getLayoutParams();
        CoordinatorLayout.Behavior behavior = params.getBehavior();

        if( behavior != null && behavior instanceof BottomSheetBehavior ) {
            ((BottomSheetBehavior) behavior).setBottomSheetCallback(mBottomSheetBehaviorCallback);
        }

        pupil = ((ProtectorApplication) getActivity().getApplication()).getPupilThatBeingRequested();
        if(pupil == null) dismiss();

        confirmButton = view.findViewById(R.id.confirm);
        passwordInput = view.findViewById(R.id.password);

        setupViews();
    }

    private void setupViews() {
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(passwordInput.getText().length() < 1) {
                    passwordInput.setError("Password is needed!");
                    return;
                } else {
                    String writtenPassword = passwordInput.getText().toString();
                    if(!PasswordsForPupils.comparePasswords(pupil, writtenPassword, getContext())) {
                        passwordInput.setError("Wrong password");
                    } else {
                        SmsCommuniation.ForGuardian.sendLocationRequest(pupil, getContext());
                    }
                }

            }
        });
    }
}
