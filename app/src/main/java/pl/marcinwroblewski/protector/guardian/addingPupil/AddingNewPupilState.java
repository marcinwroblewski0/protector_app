package pl.marcinwroblewski.protector.guardian.addingPupil;

import org.json.JSONException;
import org.json.JSONObject;

import pl.marcinwroblewski.protector.pupil.Pupil;

/**
 * Created by Marcin Wróblewski on 16.01.2017.
 */

public class AddingNewPupilState {

    public static final String CONTEXT = "guardian";

    public static final String ASKED_PUPIL = "askedPupil";
    public static final String OPERATION_STATE = "operationState";

    public static final int ASKED_PUPIL_FOR_ADDING = 100;
    public static final int WAITING_FOR_PUPIL_RESPONSE_ABOUT_ADDING = 301;
    public static final int PUPIL_ACCEPTED = 101;
    public static final int PUPIL_REJECTED = 901;
    public static final int PASSWORD_SENT_TO_PUPIL = 102;
    public static final int WAITING_FOR_PUPIL_RESPONSE_ABOUT_PASSWORD = 302;
    public static final int PUPIL_ACCEPTED_PASSWORD = 103;
    public static final int PUPIL_REJECTED_PASSWORD = 903;
    public static final int PUPIL_ADDED_GUARDIAN = 104;
    public static final int ADDING_DONE = 200;
    public static final int ADDING_REJECTED = 300;

    private Pupil askedPupil;
    private int operationState;


    public AddingNewPupilState(Pupil askedPupil, int operationState) {
        this.askedPupil = askedPupil;
        this.operationState = operationState;
    }

    public Pupil getAskedPupil() {
        return askedPupil;
    }

    public int getOperationState() {
        return operationState;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject stateJSON = new JSONObject();
        stateJSON.put(ASKED_PUPIL, getAskedPupil().toJSONObject());
        stateJSON.put(OPERATION_STATE, getOperationState());
        return stateJSON;
    }

    public static AddingNewPupilState fromJSONObject(JSONObject object) throws JSONException {
        return new AddingNewPupilState(
                Pupil.fromJSONObject(object.getJSONObject(ASKED_PUPIL)),
                object.getInt(OPERATION_STATE));
    }
}
