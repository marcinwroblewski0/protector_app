package pl.marcinwroblewski.protector.guardian;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Marcin Wróblewski on 14.01.2017.
 */

public class Guardian {

    public static final String GUARDIAN_PHONE_NUMBER = "guardianPhoneNumber";
    public static final String GUARDIAN_FIRST_NAME = "guardianFirstName";
    public static final String GUARDIAN_LAST_NAME = "guardianLastName";

    private String firstName, lastName, phoneNumber;

    public Guardian(String phoneNumber, String firstName, String lastName) {
        this.phoneNumber = phoneNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject object = new JSONObject();
        object.put(GUARDIAN_PHONE_NUMBER, getPhoneNumber());
        object.put(GUARDIAN_FIRST_NAME, getFirstName());
        object.put(GUARDIAN_LAST_NAME, getLastName());
        return object;
    }

    public static Guardian fromJSONObject(JSONObject pupilJSONObject) throws JSONException {
        return new Guardian(
                pupilJSONObject.getString(GUARDIAN_PHONE_NUMBER),
                pupilJSONObject.getString(GUARDIAN_FIRST_NAME),
                pupilJSONObject.getString(GUARDIAN_LAST_NAME)
        );
    }

    public static boolean areTheSame(Guardian one, Guardian two) {
        boolean isFirstNamesTheSame =
                one.getFirstName().equals(two.getFirstName());
        boolean isLastNamesTheSame =
                one.getLastName().equals(two.getLastName());
        boolean isPhoneNumbersTheSame =
                one.getPhoneNumber().equals(two.getPhoneNumber());

        return isFirstNamesTheSame && isLastNamesTheSame && isPhoneNumbersTheSame;
    }

}
