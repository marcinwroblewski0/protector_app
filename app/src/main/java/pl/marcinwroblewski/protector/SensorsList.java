package pl.marcinwroblewski.protector;

import android.content.Context;
import android.content.SharedPreferences;

/**
   Created by Marcin Wróblewski on 22.05.16.
 */
public class SensorsList {

    private Context context;
    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    public SensorsList(Context context) {
        this.context = context;
        this.sharedPreferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        this.editor = sharedPreferences.edit();
    }

    public void addSensor(int type, String name) {
        editor.putString("sensor" + type, name);
        editor.apply();
    }

    public String getSensorName(int type) {
        return sharedPreferences.getString("sensor" + type, "-");
    }
}
