package pl.marcinwroblewski.protector;

import android.app.IntentService;
import android.app.Notification;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import java.util.ArrayList;

import pl.marcinwroblewski.protector.configuration.Configuration;

public class DangerService extends IntentService implements SensorEventListener {

    private float[] lastSensorValue;
    private int maxX, maxY, maxZ;
    private ArrayList<ArrayList<Float>> gravitySensorDataDump, accelerometerSensorDataDump;
    private AccelerationSensorHandler accelerationSensorHandler;
    private SensorManager sensorManager;
    private SensorsList sensorsList;
    private DataAnalytics dataAnalytics;
    private SharedPreferences sp;
    private Configuration configuration;
    private DangerDetectionHelper dangerDetectionHelper;
    private ProtectorApplication application;
    private Runnable checkLocationSettings = new Runnable() {
        @Override
        public void run() {
            if(!LocationServicesCheck.isLocationEnabled(getApplicationContext())) {
                NotificationsManager.showNotification(getApplicationContext(), "Invalid app configuration", "Tap to fix", 9999);
            }

            handler.postDelayed(this, 10 * 1000);
        }
    };
    private Handler handler;

    public DangerService(String name) {
        super(name);
    }

    public DangerService() {
        super("DangerService");
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        NotificationsManager.showNotification(getApplicationContext(), "Terminated", "App has been closed");
        sensorManager.unregisterListener(this);
        Log.d("Service", "Destroyed");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        startInForeground("Active", "Looking for danger...");
        Log.d("Service", "Started");

        handler = new Handler();
        handler.postDelayed(checkLocationSettings, 10 * 1000);

        application = (ProtectorApplication) getApplication();

        configuration = new Configuration(getApplicationContext());

        if(configuration.usingOnlyAccelerometer())
            dangerDetectionHelper = new DangerDetectionHelper(getApplicationContext(), DangerDetectionHelper.MODE_ACCELEROMETER);
        else
            dangerDetectionHelper = new DangerDetectionHelper(getApplicationContext(), DangerDetectionHelper.MODE_ACCELEROMETER_AND_GYROSCOPE);


        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        if (sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST)) {
            Log.d("Accelerometer", "Registered: " + sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER).getName());
        }

        sensorsList = new SensorsList(getApplicationContext());

        gravitySensorDataDump = new ArrayList<>();
        accelerometerSensorDataDump = new ArrayList<>();
        dataAnalytics = new DataAnalytics();

        sp = getApplicationContext().getSharedPreferences(getApplicationContext()
                .getPackageName(), MODE_PRIVATE);

        accelerationSensorHandler = new AccelerationSensorHandler(getApplicationContext());
        return START_STICKY_COMPATIBILITY;
    }

    private void startInForeground(String title, String description) {
        Notification foregroundNotification = NotificationsManager.getForegroundNotification(
                    getApplicationContext(), title, description);

        startForeground(69, foregroundNotification);
    }

    boolean gravityBusy = false, accelerometerBusy = false;

    @Override
    public void onSensorChanged(final SensorEvent event) {

        if(!LocationServicesCheck.isLocationEnabled(getApplicationContext())) {

        }

        if(event.sensor.getName().equals(sensorsList.getSensorName(Sensor.TYPE_ACCELEROMETER))){
            if(application.isDangerDetectionTurnedOn() && !DataAnalytics.isTheSame(event.values, lastSensorValue)) {

                lastSensorValue = event.values.clone();
                if (maxX < lastSensorValue[0] || maxY < lastSensorValue[1] || maxZ < lastSensorValue[2]) {
                    if (maxX < lastSensorValue[0])
                        maxX = (int) lastSensorValue[0];

                    if (maxY < lastSensorValue[1])
                        maxY = (int) lastSensorValue[1];

                    if (maxZ < lastSensorValue[2])
                        maxZ = (int) lastSensorValue[2];
                }


                if (accelerationSensorHandler.detectDanger(lastSensorValue)
                        && application.isDangerDetectionTurnedOn()) {
                    dangerDetectionHelper.startCollectingData();

                    if(!configuration.usingOnlyAccelerometer())
                        if (sensorManager.registerListener(this,
                            sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY),
                            SensorManager.SENSOR_DELAY_FASTEST)) {

                            Log.d("Gyroscope", "Registered: " + sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY).getName());

                        }
                }

            }

            if(application.isCollectingData()) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        if(!accelerometerBusy) {
                            Log.d("DangerService", "Gathering data");
                            accelerometerBusy = true;

                            try {

                                for (int i = 0; i < configuration.getAnalysisTime(); i++) {
                                    Thread.sleep(250);
                                    ArrayList<Float> currentData = new ArrayList<>();


                                    for (float value : event.values) {
                                        if (sp.getBoolean("logs", false))
                                            Log.d("DangerSevice", "Adding " + value + " to " + i + " accelerometer ArrayList");
                                        currentData.add(value);
                                    }

                                    accelerometerSensorDataDump.add(currentData);

                                    if (sp.getBoolean("developer", false))
                                        startInForeground("Getting nervous", "I have bad feelings "
                                            + i + "/" + configuration.getAnalysisTime());
                                    else
                                        startInForeground("Getting nervous", "I have bad feelings");

                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();
            }
        } else if(event.sensor.getName().equals(sensorsList.getSensorName(Sensor.TYPE_GRAVITY))) {

            if(!configuration.usingOnlyAccelerometer() && application.isCollectingData()) {

                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        if(!gravityBusy) {
                            Log.d("DangerService", "Gathering data");
                            gravityBusy = true;
                            try {

                                for(int i = 0; i < configuration.getAnalysisTime(); i++) {
                                    Thread.sleep(250);
                                    ArrayList<Float> currentData = new ArrayList<>();

                                    for (float value : event.values) {
                                        if (sp.getBoolean("logs", false))
                                            Log.d("DangerSevice", "Adding " + value + " to " + i + " gravity ArrayList");
                                        currentData.add(value);
                                    }

                                    gravitySensorDataDump.add(currentData);
                                }
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                    }
                }).start();
            }
        }

        if(application.isCollectingData() &&
                (gravitySensorDataDump.size() >= configuration.getAnalysisTime() || configuration.usingOnlyAccelerometer())
                && accelerometerSensorDataDump.size() >= configuration.getAnalysisTime()) {
            int suspiciousLevel;

            //make sure that result is max 20
            if(configuration.usingOnlyAccelerometer()) {
                suspiciousLevel = dataAnalytics.accelerometerSensor(accelerometerSensorDataDump) * 2;
            } else {
                suspiciousLevel = dataAnalytics.gravitySensor(gravitySensorDataDump) +
                        dataAnalytics.accelerometerSensor(accelerometerSensorDataDump);
            }

            Log.d("DangerService", "Senors data analysis: " + suspiciousLevel + "/" + configuration.getMaxDangerPoints());
            needHelp(suspiciousLevel);
            gravityBusy = false;
            accelerometerBusy = false;
            dangerDetectionHelper.endCollectingData();
            gravitySensorDataDump = new ArrayList<>();
            accelerometerSensorDataDump = new ArrayList<>();

            if(!configuration.usingOnlyAccelerometer())
                sensorManager.unregisterListener(this,
                    sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY));



            startInForeground("Active", "Looking for danger...");
        }
    }

    private boolean needHelp(int suspiciousLevel) {

        if(DataAnalytics.isDangerPointsLevelDangerous(suspiciousLevel, getApplicationContext())) {
            NotificationsManager.showDangerNotification(getApplication());
            startInForeground("Active", "Looking for danger...");
        }

        return DataAnalytics.isDangerPointsLevelDangerous(suspiciousLevel, getApplicationContext());
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d(sensor.getName(), "Accuracy changed: " + accuracy);
    }

}
