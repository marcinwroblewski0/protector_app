package pl.marcinwroblewski.protector;

import android.content.Context;
import android.util.Log;

import java.util.ArrayList;

import pl.marcinwroblewski.protector.configuration.Configuration;

/**
   Created by Marcin Wróblewski on 20.05.2016.

 */
public class DataAnalytics {



    private ArrayList<Float> gravitySensorLastValues;
    private ArrayList<Float> accelerometerSensorLastValues;

    public DataAnalytics() {
        gravitySensorLastValues = new ArrayList<>();
        accelerometerSensorLastValues = new ArrayList<>();

        for(int i=0; i<3; i++) {
            gravitySensorLastValues.add(0f);
            accelerometerSensorLastValues.add(0f);
        }
    }

    public static boolean isTheSame(float[] data1, float[] data2) {

        if(data1 == null || data2 == null || data1.length != data2.length)
            return false;

        for(int i = 0; i < data2.length; i++) {

            if((int)data1[i] != (int)data2[i]) return false;
        }

        return true;
    }

    public int gravitySensor(ArrayList<ArrayList<Float>> data) {
        float suspiciousLevel = 0f;

        //30 (or -30) is the max value that analysis can provide because it have 3 axis (X, Y, Z)
        // and each of it can get 10 points
        float point = (10f/3f)/(float)data.size();

        Log.d("DataAnalytics", "Gravity give " + data.size() + " values and that means that 1 point has value of " + point);

        for(int i = 0; i<data.size(); i++) {
            ArrayList<Float> dataInPointOfTime = data.get(i);

            for(int j = 0; j < dataInPointOfTime.size(); j++) {

                float difference = Math.abs(gravitySensorLastValues.get(j)) - Math.abs(dataInPointOfTime.get(j));
                difference = Math.abs(difference);

                if(difference < .5f) {
                    suspiciousLevel += point;
                } else {
                    suspiciousLevel -= point;
                }
            }
            gravitySensorLastValues = dataInPointOfTime;
        }

        Log.d("DataAnalytics", "Gravity sensor thinks it's " + (int)suspiciousLevel + "/10 suspicious");
        System.out.println("Gravity sensor thinks it's " + (int)suspiciousLevel + "/10 suspicious");

        return (int)suspiciousLevel;
    }

    public int accelerometerSensor(ArrayList<ArrayList<Float>> data) {
        float suspiciousLevel = 0f;

        Log.d("DataAnalytics", "Accelerometer give " + data.size() + " values");

        //30 (or -30) is the max value that analysis can provide because it have 3 axis (X, Y, Z)
        // and each of it can get 10 points
        float point = (10f/3f)/(float)data.size();

        Log.d("DataAnalytics", "Accelerometer give " + data.size() + " values and that means that 1 point has value of " + point);

        for(int i = 0; i < data.size(); i++) {
            ArrayList<Float> dataInPointOfTime = data.get(i);

            Log.d("DataAnalytics", "DataInPointOfTime size: " + dataInPointOfTime.size());

            for(int j = 0; j < dataInPointOfTime.size(); j++) {
                Log.d("DataAnalytics", "Hi2");
                float difference = Math.abs(accelerometerSensorLastValues.get(j)) - Math.abs(dataInPointOfTime.get(j));
                difference = Math.abs(difference);

                if(difference < 1.5f) {
                    suspiciousLevel += point;
                    Log.d("DataAnalytics", "Giving point because " + difference);
                } else {
                    suspiciousLevel -= point;
                    Log.d("DataAnalytics", "Removing point because " + difference);
                }
            }
            accelerometerSensorLastValues = dataInPointOfTime;
        }

        Log.d("DataAnalytics", "Accelerometer sensor thinks it's " + (int)suspiciousLevel + "/10 suspicious");
        System.out.println("Accelerometer sensor thinks it's " + (int)suspiciousLevel + "/10 suspicious");
        return (int)suspiciousLevel;
    }

    public static boolean isDangerPointsLevelDangerous(int dangerPoints, Context context) {
        Configuration configuration = new Configuration(context);

        System.out.println("Max danger points: " + configuration.getMaxDangerPoints());

        return dangerPoints >= configuration.getMaxDangerPoints();

    }

    public static boolean isDangerPointsLevelDangerous(int dangerPoints, int maxDangerPoints) {
        return dangerPoints >= maxDangerPoints;
    }
}
