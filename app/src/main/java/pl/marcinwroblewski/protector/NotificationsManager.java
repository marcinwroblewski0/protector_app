package pl.marcinwroblewski.protector;

import android.app.Application;
import android.app.IntentService;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import pl.marcinwroblewski.protector.configuration.Configuration;

/**
   Created by Marcin Wróblewski on 19.05.2016.
 */
public class NotificationsManager {

    static boolean itsOk;
    public static final String TURN_OFF_SOUND = "action_1";
    public static final String FALSE_ALARM = "action_2";
    private static int notificationCounter = 10000;

    public static void showDangerNotification(final Application application) {
        ProtectorApplication pApp = (ProtectorApplication) application;
        Context context = pApp.getApplicationContext();
        itsOk = false;

        long[] vibratePattern = {200, 800};

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);

        NotificationsManager nm = new NotificationsManager();

        Intent action2Intent = new Intent(context, NotificationActionService.class)
                .setAction(FALSE_ALARM);

        PendingIntent action2PendingIntent = PendingIntent.getService(context, 0,
                action2Intent, PendingIntent.FLAG_ONE_SHOT);

        final NotificationCompat.Builder builder =
            new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_stat_shield)
                .setContentTitle("Danger detected")
                .setContentText("Dismiss if it's mistake")
                .setVibrate(vibratePattern)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setColor(0xd50000)
                .setLights(0xff0000, 100, 100)
                .setSound(alarmSound)
                .setDeleteIntent(nm.createOnDismissedIntent(context, 0))
                .addAction(new NotificationCompat.Action(R.drawable.ic_stop,
                            "False alarm", action2PendingIntent));

        final NotificationManagerCompat notificationManager
                = (NotificationManagerCompat.from(context));

        notificationManager.notify(0, builder.build());

        final Configuration configuration = new Configuration(context);
        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i = configuration.getCountdownTime(); i > 0; i--) {
                    if(itsOk) return;
                    builder.setContentTitle("Danger detected " + i);
                    notificationManager.notify(0, builder.build());
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                notificationManager.cancel(0);

                if(!itsOk) {
                    AlertManager alertManager = new AlertManager(application);
                    alertManager.triggerAlarm();
                }
            }
        }).start();
    }

    public static void showNotification(Context context, String title, String content) {

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_stat_shield)
                        .setContentTitle(title)
                        .setContentText(content)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setColor(0xfdd835)
                        .setContentIntent(NotificationsManager.getPendingIntentForMainActivity(context));

        NotificationManagerCompat notificationManager
                = (NotificationManagerCompat.from(context));

        notificationManager.notify(notificationCounter, builder.build());
        notificationCounter++;
    }

    public static void showNotification(Context context, String title, String content, int id) {

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_stat_shield)
                        .setContentTitle(title)
                        .setContentText(content)
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                        .setColor(0xfdd835)
                        .setContentIntent(NotificationsManager.getPendingIntentForMainActivity(context));

        NotificationManagerCompat notificationManager
                = (NotificationManagerCompat.from(context));

        notificationManager.notify(id, builder.build());
    }

    public static Notification getNotification(Context context, String title, String content) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_stat_shield)
                        .setContentTitle(title)
                        .setContentText(content)
                        .setPriority(NotificationCompat.PRIORITY_MIN)
                        .setColor(0xfdd835)
                        .setContentIntent(NotificationsManager.getPendingIntentForMainActivity(context));

        return builder.build();
    }

    public static Notification getForegroundNotification(Context context, String title, String content) {
        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_stat_shield)
                        .setContentTitle(title)
                        .setContentText(content)
                        .setPriority(NotificationCompat.PRIORITY_MIN)
                        .setColor(0x388e3c)
                        .setContentIntent(NotificationsManager.getPendingIntentForMainActivity(context));

        return builder.build();
    }

    private static PendingIntent getPendingIntentForMainActivity(Context context) {
        Intent intent = new Intent(context, MainActivity.class);
        return PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private PendingIntent createOnDismissedIntent(Context context, int notificationId) {
        Intent intent = new Intent(context, NotificationDismissedReceiver.class);

        return PendingIntent.getBroadcast(context.getApplicationContext(),
                notificationId, intent, 0);
    }

    public static final int NOTIFICATION_ID = 666;


    public static void displayTurnOffNotification(Context context) {

        Intent action1Intent = new Intent(context, NotificationActionService.class)
                .setAction(TURN_OFF_SOUND);

        PendingIntent action1PendingIntent = PendingIntent.getService(context, 0,
                action1Intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder builder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_stat_shield)
                        .setContentTitle("Alarm rised!")
                        .setPriority(NotificationCompat.PRIORITY_MAX)
                        .setColor(0xd50000)
                        .addAction(new NotificationCompat.Action(R.drawable.ic_volume_off,
                                "Turn off", action1PendingIntent));

        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);
        notificationManager.notify(NOTIFICATION_ID, builder.build());
    }

    public static class NotificationActionService extends IntentService {
        public NotificationActionService() {
            super(NotificationActionService.class.getSimpleName());
        }

        @Override
        protected void onHandleIntent(Intent intent) {
            String action = intent.getAction();
            Log.d("NotificationManager", "Received notification action: " + action);
            if (TURN_OFF_SOUND.equals(action)) {
                NotificationManagerCompat.from(this).cancel(NOTIFICATION_ID);
                ((ProtectorApplication) getApplication()).turnOffAlarmSound();
            } else if (FALSE_ALARM.equals(action)) {
                NotificationManagerCompat.from(this).cancel(0);
                itsOk = true;
            }
        }
    }
}
