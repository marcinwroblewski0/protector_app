package pl.marcinwroblewski.protector.pupil.guardiansList;


import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.tapadoo.alerter.Alerter;

import pl.marcinwroblewski.protector.ConfigurationSmsSniffer;
import pl.marcinwroblewski.protector.R;
import pl.marcinwroblewski.protector.guardian.Guardian;
import pl.marcinwroblewski.protector.pupil.addingGuardian.AddingNewGuardianState;
import pl.marcinwroblewski.protector.pupil.addingGuardian.GuardianAddingOperationsList;

public class GuardianListFragment extends Fragment {

    private RecyclerView recyclerView;
    private GuardianListAdapter adapter;
    private AddingNewGuardianState[] states;
    private View lookForConfigurationMessagesInInboxButton;
    private LookForConfigurationMessagesAsyncTask asyncTask;
    private Handler handler;
    private Runnable updateTask = new Runnable() {
        @Override
        public void run() {
            if(getContext() != null) {
                states = GuardianAddingOperationsList.getAllOperationsState(getContext());
                recyclerView.setAdapter(new GuardianListAdapter(states, getContext()));
            }
            handler.postDelayed(this, 5000);
        }
    };

    public GuardianListFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = new Handler();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        states = GuardianAddingOperationsList.getAllOperationsState(getContext());

        View rootView =  inflater.inflate(R.layout.fragment_guardian_list, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.guardians_recycler_view);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 1);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        adapter = new GuardianListAdapter(states, getContext());
        recyclerView.setAdapter(adapter);

        View addGuardianButton = rootView.findViewById(R.id.add_guardian);
        addGuardianButton.setClickable(true);
        addGuardianButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "Not ready yet", Toast.LENGTH_SHORT).show();
            }
        });

        handler.post(updateTask);

        lookForConfigurationMessagesInInboxButton = rootView.findViewById(R.id.look_for_configuration_messages_button);
        lookForConfigurationMessagesInInboxButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lookForConfigurationMessages();
            }
        });

        return rootView;
    }

    private void lookForConfigurationMessages() {
        LookForConfigurationMessagesAsyncTask asyncTask = new LookForConfigurationMessagesAsyncTask();
        asyncTask.execute();
        showTaskRunning();
    }

    private void showTaskRunning() {
        Alerter.create(getActivity())
                .setTitle("Please wait")
                .setText("Looking for configuration messages in inbox...")
                .show();
    }

    private void hideTaskRunning() {
        Alerter.create(getActivity())
                .setTitle("Done!")
                .setText("Refreshing finished successfully")
                .show();
        refreshList();
    }

    private void refreshList() {
        states = GuardianAddingOperationsList.getAllOperationsState(getContext());
        adapter.notifyDataSetChanged();
    }

    private class LookForConfigurationMessagesAsyncTask extends AsyncTask<Void, Void, Void> {
        @Override
        protected Void doInBackground(Void... voids) {
            ConfigurationSmsSniffer smsSniffer = new ConfigurationSmsSniffer(getContext());
            for (Guardian guardian : smsSniffer.findRequestingGuardians()) {
                smsSniffer.restoreRequestStatus(
                        smsSniffer.extractConversation(guardian.getPhoneNumber()),
                        guardian);
            }

            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hideTaskRunning();
                }
            });
            return null;
        }
    }
}
