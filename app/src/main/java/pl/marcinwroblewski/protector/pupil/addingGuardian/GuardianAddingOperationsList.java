package pl.marcinwroblewski.protector.pupil.addingGuardian;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import pl.marcinwroblewski.protector.OperationResult;
import pl.marcinwroblewski.protector.guardian.Guardian;

/**
 * Created by Marcin Wróblewski on 16.01.2017.
 */

public class GuardianAddingOperationsList {

    public static final String ALL_GUARDIAN_REQUESTS_OPERATIONS = "allGuardianRequestingOperations";

    public static OperationResult addNewOperation(AddingNewGuardianState state, Context context) {

        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Set<String> allOperations = getAllOperationsStringSet(context);


        if(isAlreadyInList(state, context)) return new OperationResult(OperationResult.STATUS_ERROR,
                "State is already in list");

        try {
            if(!allOperations.add(state.toJSONObject().toString())) {
                return new OperationResult(
                        OperationResult.STATUS_ERROR,
                        "Cannot add state to set: \n" + state.toJSONObject());
            }
            editor.putStringSet(ALL_GUARDIAN_REQUESTS_OPERATIONS, allOperations);
            if(!editor.commit()) {
                return new OperationResult(
                        OperationResult.STATUS_ERROR,
                        "Cannot commit changes: \n" + state.toJSONObject());
            }
            return new OperationResult(
                    OperationResult.STATUS_SUCCESS,
                    "Adding operation went successfully: \n" + state.toJSONObject());
        } catch (JSONException e) {
            return new OperationResult(
                    OperationResult.STATUS_ERROR,
                    "Cannot add new operation to list: \n"
                            + e.getMessage());

        }
    }

    public static OperationResult removeOperation(AddingNewGuardianState state, Context context) {

        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();

        Set<String> allOperations = getAllOperationsStringSet(context);

        if(!isAlreadyInList(state, context)) return new OperationResult(OperationResult.STATUS_ERROR,
                "Cannot remove operation that's not in list");

        try {
            if (!allOperations.remove(state.toJSONObject().toString())) {
                return new OperationResult(
                        OperationResult.STATUS_ERROR,
                        "Cannot delete from set: \n" + state.toJSONObject().toString());
            }

            Log.d("Operations", "After remove " + state.toJSONObject().toString());
            for (String operation : getAllOperationsStringSet(context)) {
                Log.d("Operations", operation);
            }

            editor.putStringSet(ALL_GUARDIAN_REQUESTS_OPERATIONS, allOperations);
            editor.commit();
            return new OperationResult(
                    OperationResult.STATUS_SUCCESS,
                    "Removing operation went successfully: \n" + state.toJSONObject());
        } catch (JSONException e) {
            return new OperationResult(
                    OperationResult.STATUS_ERROR,
                    "Cannot remove operation to list: \n"
                            + e.getMessage());

        }
    }

    public static boolean isAlreadyInList(AddingNewGuardianState state, Context context) {
        Set<String> allOperations = getAllOperationsStringSet(context);

        Log.d("Guardian", "Guardians in list: " + allOperations.size());

        for (String operationString : allOperations) {
            try {
                JSONObject operationObject = new JSONObject(operationString);
                Guardian operationObjectGuardian = Guardian.fromJSONObject(
                        operationObject.getJSONObject(AddingNewGuardianState.REQUESTING_GUARDIAN));

                if (Guardian.areTheSame(operationObjectGuardian, state.getRequestingGuardian()))
                    return true;

                if(operationObjectGuardian.getPhoneNumber().equals(state.getRequestingGuardian().getPhoneNumber())) {
                    return true;
                }

            } catch (JSONException e) {
                e.printStackTrace();
                return false;
            }
        }

        return false;
    }

    public static HashSet<String> getAllOperationsStringSet(Context context) {

        SharedPreferences preferences = context.getSharedPreferences(context.getPackageName(),
                Context.MODE_PRIVATE);

        //using object strait from .getStringSet is forbidden in the docs
        Set<String> sharedPrefsSet = preferences.getStringSet(ALL_GUARDIAN_REQUESTS_OPERATIONS,
                new HashSet<String>());
        HashSet<String> operations = new HashSet<String>();
        operations.addAll(sharedPrefsSet);


        return operations;
    }

    public static AddingNewGuardianState[] getAllOperationsState(Context context) {

        Set<String> allOperationsSet = getAllOperationsStringSet(context);
        AddingNewGuardianState[] newGuardianStates = new AddingNewGuardianState[allOperationsSet.size()];

        int i = 0;
        for (String operation : allOperationsSet) {
            try {
                newGuardianStates[i] = AddingNewGuardianState.fromJSONObject(
                        new JSONObject(operation));
                i++;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return newGuardianStates;
    }

    public static AddingNewGuardianState getOperationFor(Guardian guardian, Context context) {

        for (AddingNewGuardianState stateInList : getAllOperationsState(context)) {
            if (Guardian.areTheSame(stateInList.getRequestingGuardian(), guardian))
                return stateInList;
        }

        return null;
    }

    public static OperationResult modifyOperation(Guardian guardian, int newState, Context context) {
        AddingNewGuardianState stateToModify = getOperationFor(guardian, context);

        if(stateToModify == null)
            return new OperationResult(OperationResult.STATUS_ERROR,
                    "There's no operation for provided guardian");

        AddingNewGuardianState modifiedState = new AddingNewGuardianState(guardian, newState);


        OperationResult removeOperationResult = removeOperation(stateToModify, context);
        if(removeOperationResult.getStatus().equals(OperationResult.STATUS_ERROR)) {
            return removeOperationResult;
        }

        OperationResult addingOperationResult = addNewOperation(modifiedState, context);
        if(addingOperationResult.getStatus().equals(OperationResult.STATUS_ERROR)) {
            return addingOperationResult;
        }


        return new OperationResult(OperationResult.STATUS_SUCCESS,
                "Operation modified successfully");
    }

}
