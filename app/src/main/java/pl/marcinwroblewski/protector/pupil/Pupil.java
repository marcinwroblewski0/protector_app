package pl.marcinwroblewski.protector.pupil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Admin on 14.01.2017.
 */

public class Pupil {

    public static final String PUPIL_PHONE_NUMBER = "pupilPhoneNumber";
    public static final String PUPIL_FIRST_NAME = "pupilFirstName";
    public static final String PUPIL_LAST_NAME = "pupilLastName";

    private String phoneNumber, firstName, lastName;

    public Pupil(String phoneNumber, String firstName, String lastName) {
        this.phoneNumber = phoneNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject object = new JSONObject();
        object.put(PUPIL_PHONE_NUMBER, getPhoneNumber());
        object.put(PUPIL_FIRST_NAME, getFirstName());
        object.put(PUPIL_LAST_NAME, getLastName());
        return object;
    }

    public static Pupil fromJSONObject(JSONObject pupilJSONObject) throws JSONException {
        return new Pupil(
                pupilJSONObject.getString(PUPIL_PHONE_NUMBER),
                pupilJSONObject.getString(PUPIL_FIRST_NAME),
                pupilJSONObject.getString(PUPIL_LAST_NAME)
        );
    }

    public static boolean areTheSame(Pupil one, Pupil two) {
        boolean isFirstNamesTheSame =
                one.getFirstName().equals(two.getFirstName());
        boolean isLastNamesTheSame =
                one.getLastName().equals(two.getLastName());
        boolean isPhoneNumbersTheSame =
                one.getPhoneNumber().equals(two.getPhoneNumber());

        return isFirstNamesTheSame && isLastNamesTheSame && isPhoneNumbersTheSame;
    }
}
