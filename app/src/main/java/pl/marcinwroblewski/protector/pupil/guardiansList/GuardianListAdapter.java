package pl.marcinwroblewski.protector.pupil.guardiansList;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import pl.marcinwroblewski.protector.R;
import pl.marcinwroblewski.protector.SmsCommuniation;
import pl.marcinwroblewski.protector.pupil.addingGuardian.AddingNewGuardianState;

import static android.view.View.GONE;

/**
 * Created by Marcin Wróblewski on 18.01.2017.
 */

public class GuardianListAdapter extends RecyclerView.Adapter<GuardianListAdapter.MyViewHolder> {

    private AddingNewGuardianState[] states;
    private Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView guardianName, guardianPhoneNumber;
        public ImageView imageState;
        public LinearLayout decisionContainer;

        public MyViewHolder(View view) {
            super(view);
            guardianName = (TextView) view.findViewById(R.id.guardian_name);
            guardianPhoneNumber = (TextView) view.findViewById(R.id.guardian_phone_number);
            imageState = (ImageView) view.findViewById(R.id.state_image);
            decisionContainer = (LinearLayout) view.findViewById(R.id.decision_container);
        }
    }


    public GuardianListAdapter(AddingNewGuardianState[] states, Context context) {
        this.states = states;
        this.context = context;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.guardian_list_element, parent, false);

        return new MyViewHolder(itemView);
    }


    @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            holder.guardianName.setText(states[position].getRequestingGuardian().getFirstName() + " " +
                    states[position].getRequestingGuardian().getLastName());
            holder.guardianPhoneNumber.setText(states[position].getRequestingGuardian().getPhoneNumber());


            switch (states[position].getOperationState()) {
                case AddingNewGuardianState.ADDING_DONE:
                    holder.imageState.setImageResource(R.drawable.ic_added);
                    break;
                case AddingNewGuardianState.WAITING_FOR_PASSWORD_FROM_GUARDIAN:
                    holder.imageState.setImageResource(R.drawable.ic_pending);
                    break;
                case AddingNewGuardianState.ADDING_REQUEST:
                    holder.imageState.setImageResource(R.drawable.ic_pending);
                    prepareDecisionContainer(states[position], holder.decisionContainer);
                    break;
                case AddingNewGuardianState.ADDING_REJECTED:
                    holder.imageState.setImageResource(R.drawable.ic_rejected);
                    break;
            }
        }

    private void prepareDecisionContainer(final AddingNewGuardianState state, final LinearLayout container) {
        container.setVisibility(View.VISIBLE);

        View acceptButton = container.findViewById(R.id.accept_button);
        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SmsCommuniation.ForPupil.sendRequestResponse(state.getRequestingGuardian(),
                        AddingNewGuardianState.ACCEPT_REQUEST, context);

                container.setVisibility(GONE);
            }
        });

        View rejectButton = container.findViewById(R.id.reject_button);
        rejectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SmsCommuniation.ForPupil.sendRequestResponse(state.getRequestingGuardian(),
                        AddingNewGuardianState.REJECT_REQUEST, context);

                container.setVisibility(GONE);
            }
        });
    }

        @Override
        public int getItemCount() {
            return states.length;
        }

}
