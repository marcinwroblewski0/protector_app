package pl.marcinwroblewski.protector.pupil;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import pl.marcinwroblewski.protector.OperationResult;

/**
 * Created by Marcin Wróblewski on 14.01.2017.
 */

public class PupilList {

    public static final String PUPIL_LIST = "pupilList";

    public static OperationResult addPupil(Pupil pupil, Context context) {

        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE
        );
        SharedPreferences.Editor editor = preferences.edit();

        Set<String> pupilsList = getStringSetOfAllPupils(context);

        for (String pupilString : pupilsList) {
            try {
                JSONObject pupilJSON = new JSONObject(pupilString);
                boolean isFirstNamesTheSame =
                        pupilJSON.getString(Pupil.PUPIL_FIRST_NAME).equals(pupil.getFirstName());
                boolean isLastNamesTheSame =
                        pupilJSON.getString(Pupil.PUPIL_LAST_NAME).equals(pupil.getLastName());
                boolean isPhoneNumbersTheSame =
                        pupilJSON.getString(Pupil.PUPIL_PHONE_NUMBER).equals(pupil.getPhoneNumber());

                if(isFirstNamesTheSame && isLastNamesTheSame && isPhoneNumbersTheSame) {
                    return new OperationResult(
                            OperationResult.STATUS_ERROR,
                            "Pupil already exists");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        try {
            pupilsList.add(pupil.toJSONObject().toString());
            editor.putStringSet(PUPIL_LIST, pupilsList);
            editor.apply();
            return new OperationResult(
                    OperationResult.STATUS_SUCCESS,
                    "Pupil successfully added to list");
        } catch (JSONException e) {
            return new OperationResult(
                    OperationResult.STATUS_ERROR,
                    "Cannot convert pupil object to JSONObject and save it to the pupils list: \n"
                            + e.getMessage());

        }

    }

    public static Pupil[] getListOfAllPupils(Context context) {
        Set<String> pupilsSet = getStringSetOfAllPupils(context);
        Pupil[] pupils = new Pupil[pupilsSet.size()];

        int i = 0;
        for (String pupilString : pupilsSet) {
            try {
                pupils[i] = Pupil.fromJSONObject(new JSONObject(pupilString));
                i++;
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return pupils;
    }

    public static Set<String> getStringSetOfAllPupils(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE
        );
        return preferences.getStringSet(PUPIL_LIST, new HashSet<String>());
    }
}
