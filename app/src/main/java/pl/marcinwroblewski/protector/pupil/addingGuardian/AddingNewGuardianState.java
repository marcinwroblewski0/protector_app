package pl.marcinwroblewski.protector.pupil.addingGuardian;

import org.json.JSONException;
import org.json.JSONObject;

import pl.marcinwroblewski.protector.guardian.Guardian;

/**
 * Created by Marcin Wróblewski on 16.01.2017.
 */

public class AddingNewGuardianState {

    public static final String CONTEXT = "pupil";

    public static final String REQUESTING_GUARDIAN = "requestingGuardian";
    public static final String OPERATION_STATE = "operationState";


    public static final int ADDING_REQUEST = 100;
    public static final int ACCEPT_REQUEST = 101;
    public static final int REJECT_REQUEST = 901;
    public static final int WAITING_FOR_PASSWORD_FROM_GUARDIAN = 302;
    public static final int PASSWORD_RECEIVED = 102;
    public static final int ACCEPT_PASSWORD = 103;
    public static final int REJECT_PASSWORD = 903;
    public static final int ADDING_DONE = 200;
    public static final int ADDING_REJECTED = 1234;

    private Guardian requestingGuardian;
    private int operationState;


    public AddingNewGuardianState(Guardian requestingGuardian, int operationState) {
        this.requestingGuardian = requestingGuardian;
        this.operationState = operationState;
    }

    public Guardian getRequestingGuardian() {
        return requestingGuardian;
    }

    public int getOperationState() {
        return operationState;
    }

    public JSONObject toJSONObject() throws JSONException {
        JSONObject stateJSON = new JSONObject();
        stateJSON.put(REQUESTING_GUARDIAN, getRequestingGuardian().toJSONObject());
        stateJSON.put(OPERATION_STATE, getOperationState());
        return stateJSON;
    }

    public static AddingNewGuardianState fromJSONObject(JSONObject object) throws JSONException {
        return new AddingNewGuardianState(
                Guardian.fromJSONObject(object.getJSONObject(REQUESTING_GUARDIAN)),
                object.getInt(OPERATION_STATE));
    }
}
