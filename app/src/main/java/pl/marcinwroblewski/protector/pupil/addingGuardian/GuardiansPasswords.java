package pl.marcinwroblewski.protector.pupil.addingGuardian;

import android.content.Context;
import android.content.SharedPreferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Set;

import pl.marcinwroblewski.protector.OperationResult;
import pl.marcinwroblewski.protector.guardian.Guardian;

/**
 * Created by Marcin Wróblewski on 18.01.2017.
 */

public class GuardiansPasswords {

    private static final String ALL_PASSWORDS = "guardianPasswords";
    private static final String GUARDIAN = "guardian";
    private static final String PASSWORD = "password";

    public static OperationResult assign(Guardian guardian, String password, Context context) {

        Set<String> allPasswords = getAllPasswords(context);

        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(),
                Context.MODE_PRIVATE
        );
        SharedPreferences.Editor editor = preferences.edit();

        try {
            JSONObject guardiansPasswordObject = new JSONObject();
            guardiansPasswordObject.put(GUARDIAN, guardian.toJSONObject());
            guardiansPasswordObject.put(PASSWORD, password);
            allPasswords.add(guardiansPasswordObject.toString());
            editor.putStringSet(ALL_PASSWORDS, allPasswords);
            editor.apply();
            return new OperationResult(
                    OperationResult.STATUS_SUCCESS,
                    "Password for guardian added successfully"
            );
        } catch (JSONException e) {
            e.printStackTrace();
            return new OperationResult(
                    OperationResult.STATUS_ERROR,
                    "Cannot add password for guardian: \n" + e.getMessage()
            );
        }
    }

    public static boolean comparePasswords(Guardian guardian, String password, Context context) {

        String passwordForGuardian = getPasswordFor(guardian, context);
        if(passwordForGuardian == null) {
            return false;
        }

        if(passwordForGuardian.equals(password)) return true;
        else return false;
    }

    private static String getPasswordFor(Guardian guardian, Context context) {
        Set<String> allPasswords = getAllPasswords(context);

        for (String guardiansPasswordString : allPasswords) {
            try {
                JSONObject guardiansPasswordObject = new JSONObject(guardiansPasswordString);
                Guardian currentGuardian = Guardian.fromJSONObject(guardiansPasswordObject.getJSONObject(GUARDIAN));

                if(Guardian.areTheSame(currentGuardian, guardian)) {
                    return guardiansPasswordObject.getString(PASSWORD);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        return null;
    }

    private static Set<String> getAllPasswords(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(),
                Context.MODE_PRIVATE
        );

        return preferences.getStringSet(ALL_PASSWORDS, new HashSet<String>());
    }

}
