package pl.marcinwroblewski.protector;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Admin on 14.11.2016.
 */

public class SensitivenessSpinnerAdapter extends BaseAdapter {

    private ArrayList<Integer> images;
    private ArrayList<String> names;
    private Context context;
    private LayoutInflater inflater;

    public SensitivenessSpinnerAdapter(ArrayList<Integer> images, ArrayList<String> names, Context context) {
        this.images = images;
        this.names = names;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return names.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View spinnerItem = inflater.inflate(R.layout.sensitiveness_spinner_item, null);
        ImageView image = (ImageView) spinnerItem.findViewById(R.id.item_image);
        image.setImageResource(images.get(i));

        TextView text = (TextView) spinnerItem.findViewById(R.id.item_name);
        text.setText(names.get(i));
        return spinnerItem;
    }
}
