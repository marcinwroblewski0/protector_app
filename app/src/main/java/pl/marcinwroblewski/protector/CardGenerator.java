package pl.marcinwroblewski.protector;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.view.LayoutInflater;
import android.view.View;

public class CardGenerator {

    public static View getCardView(String name, String vendor, Context context) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View card = layoutInflater.inflate(R.layout.card_sensor, null);

        AppCompatTextView nameTV = (AppCompatTextView)card.findViewById(R.id.name);
        nameTV.setText(name);

        AppCompatTextView vendorTV = (AppCompatTextView)card.findViewById(R.id.vendor);
        vendorTV.setText(vendor);

        return card;
    }

}
