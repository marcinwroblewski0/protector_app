package pl.marcinwroblewski.protector;

import android.app.Application;
import android.app.Notification;
import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Looper;
import android.telephony.SmsManager;
import android.util.Log;

import java.util.ArrayList;

import br.com.goncalves.pugnotification.notification.PugNotification;
import pl.marcinwroblewski.protector.configuration.Configuration;
import pl.marcinwroblewski.protector.guardian.Guardian;
import pl.marcinwroblewski.protector.pupil.addingGuardian.AddingNewGuardianState;
import pl.marcinwroblewski.protector.pupil.addingGuardian.GuardianAddingOperationsList;

import static android.content.Context.LOCATION_SERVICE;
import static android.location.LocationManager.GPS_PROVIDER;
import static android.location.LocationManager.NETWORK_PROVIDER;

/**
 * Created by Marcin Wróblewski on 2016-05-19.
 */
@SuppressWarnings("MissingPermission")
public class AlertManager implements LocationListener {

    private Location lastKnownLocation;
    private String locatingErrorMessage = "";
    private LocationManager locationManager;
    private Context context;
    private ProtectorApplication application;
    private Guardian guardian;
    private SharedPreferences preferences;

    private final int DEBUG_LOCATION_NOTIFICATION = 1101;

    public AlertManager(Application application) {
        this.context = application.getApplicationContext();
        this.application = (ProtectorApplication) application;

        preferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);

        if(Looper.myLooper() == null)
            Looper.prepare();

        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        try {
            try {
                locationManager.requestLocationUpdates(GPS_PROVIDER, 0, 0, this);
                if(preferences.getBoolean("developer", false)) {
                    //NotificationsManager.showNotification(context, "Looking for location", "via GPS", DEBUG_LOCATION_NOTIFICATION);
                    PugNotification.with(application)
                            .load()
                            .title("Looking for location")
                            .message("via GPS")
                            .smallIcon(R.drawable.ic_stat_shield)
                            .flags(Notification.DEFAULT_ALL)
                            .simple()
                            .build();
                }
            } catch (IllegalArgumentException e) {
                locationManager.requestLocationUpdates(NETWORK_PROVIDER, 0, 0, this);
                if(preferences.getBoolean("developer", false)) {
                    //NotificationsManager.showNotification(context, "Looking for location", "via Network", DEBUG_LOCATION_NOTIFICATION);
                    PugNotification.with(application)
                            .load()
                            .title("Looking for location")
                            .message("via Network")
                            .smallIcon(R.drawable.ic_stat_shield)
                            .flags(Notification.DEFAULT_ALL)
                            .simple()
                            .build();
                }
                e.printStackTrace();
            }
        } catch (IllegalArgumentException e) {
            lastKnownLocation = new Location("");
            locatingErrorMessage = ". \n!!!Protector could not locate the phone!!!";
            if(preferences.getBoolean("developer", false)) {
                //NotificationsManager.showNotification(context, "Looking for location", "can not obtain location", DEBUG_LOCATION_NOTIFICATION);
                PugNotification.with(application)
                        .load()
                        .title("Looking for location")
                        .message("can not obtain location")
                        .smallIcon(R.drawable.ic_stat_shield)
                        .flags(Notification.DEFAULT_ALL)
                        .simple()
                        .build();
            }
            sendSMS();
            e.printStackTrace();
        }

        Looper.loop();
    }

    public AlertManager(Application application, Guardian guardian) {
        this.context = application.getApplicationContext();
        this.application = (ProtectorApplication) application;
        this.guardian = guardian;

        preferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);

        if(Looper.myLooper() == null)
            Looper.prepare();

        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        try {
            try {
                locationManager.requestLocationUpdates(GPS_PROVIDER, 0, 0, this);
                if(preferences.getBoolean("developer", false)) {
                    //NotificationsManager.showNotification(context, "Looking for location", "via GPS", DEBUG_LOCATION_NOTIFICATION);
                    PugNotification.with(application)
                            .load()
                            .title("Looking for location")
                            .message("via GPS")
                            .smallIcon(R.drawable.ic_stat_shield)
                            .flags(Notification.DEFAULT_ALL)
                            .simple()
                            .build();
                }
            } catch (IllegalArgumentException e) {
                locationManager.requestLocationUpdates(NETWORK_PROVIDER, 0, 0, this);
                if(preferences.getBoolean("developer", false)) {
                    //NotificationsManager.showNotification(context, "Looking for location", "via Network", DEBUG_LOCATION_NOTIFICATION);
                    PugNotification.with(application)
                            .load()
                            .title("Looking for location")
                            .message("via Network")
                            .smallIcon(R.drawable.ic_stat_shield)
                            .flags(Notification.DEFAULT_ALL)
                            .simple()
                            .build();
                }
                e.printStackTrace();
            }
        } catch (IllegalArgumentException e) {
            lastKnownLocation = new Location("");
            locatingErrorMessage = ". \n!!!Protector could not locate the phone!!!";
            if(preferences.getBoolean("developer", false)) {
                //NotificationsManager.showNotification(context, "Looking for location", "can not obtain location", DEBUG_LOCATION_NOTIFICATION);
                PugNotification.with(application)
                        .load()
                        .title("Looking for location")
                        .message("can not obtain location")
                        .smallIcon(R.drawable.ic_stat_shield)
                        .flags(Notification.DEFAULT_ALL)
                        .simple()
                        .build();
            }
            sendSMS();
            e.printStackTrace();
        }

        Looper.loop();
    }

    public void triggerAlarm() {
        startScreaming();
    }

    private void sendSMS() {
        Log.d("Alert Manager", "sending SMS: " + lastKnownLocation.toString());
        Configuration configuration = new Configuration(context);

        AddingNewGuardianState[] states = GuardianAddingOperationsList.getAllOperationsState(context);

        if(guardian != null) {
            String emergencyMessage = configuration.getEmergencyMessage();
            String text =
                    emergencyMessage + locatingErrorMessage
                            + "\n"
                            + "Latitude: " + lastKnownLocation.getLatitude() + ", Longtitude: " + lastKnownLocation.getLongitude()
                            + "\n"
                            + "http://maps.google.com/?q=" + lastKnownLocation.getLatitude() + "," + lastKnownLocation.getLongitude();
            SmsManager smsManager = SmsManager.getDefault();
            ArrayList<String> msgStringArray = smsManager.divideMessage(text);
            smsManager.sendMultipartTextMessage(guardian.getPhoneNumber(), null, msgStringArray, null, null);
            Log.d("Alert manager", "SMS content: \n" + text);
            NotificationsManager.showNotification(context,
                    "SMS sent to " + guardian.getPhoneNumber(),
                    "\" " + text + "\"");
        } else {

            for (AddingNewGuardianState state : states) {
                if (state.getOperationState() == AddingNewGuardianState.ADDING_DONE) {
                    String emergencyMessage = configuration.getEmergencyMessage();
                    String text =
                            emergencyMessage + locatingErrorMessage
                                    + "\n"
                                    + "Latitude: " + lastKnownLocation.getLatitude() + ", Longtitude: " + lastKnownLocation.getLongitude()
                                    + "\n"
                                    + "http://maps.google.com/?q=" + lastKnownLocation.getLatitude() + "," + lastKnownLocation.getLongitude();
                    SmsManager smsManager = SmsManager.getDefault();
                    ArrayList<String> msgStringArray = smsManager.divideMessage(text);
                    smsManager.sendMultipartTextMessage(state.getRequestingGuardian().getPhoneNumber(), null, msgStringArray, null, null);
                    Log.d("Alert manager", "SMS content: \n" + text);
                    NotificationsManager.showNotification(context,
                            "SMS sent to " + state.getRequestingGuardian().getPhoneNumber(),
                            "\" " + text + "\"");
                }
            }

        }
        cleanUp();
    }

    private void cleanUp() {

        //noinspection MissingPermission
        locationManager.removeUpdates(this);
        try {
            Looper myLooper = Looper.myLooper();
            if (myLooper != null) {
                myLooper.quit();
            }
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    public void startScreaming() {
        NotificationsManager.displayTurnOffNotification(context);
        application.turnOnAlarmSound();
    }

    @Override
    public void onLocationChanged(Location location) {
        if(location == null) {
            Log.e("Location Manager", "Location was null");
            if(preferences.getBoolean("developer", false)) {
                NotificationsManager.showNotification(context, "Location Manager", "Location was null");
            }
        }
        Log.d("Location Manager", "Last known location: " + location.toString());
        lastKnownLocation = location;
        if(preferences.getBoolean("developer", false)) {
            NotificationsManager.showNotification(context, "Location obtained", ": " + location.toString());
        }
        sendSMS();
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {
        assert bundle != null;
        Log.d("Location Manager", "status changed " + s + ", " + i + ", " + bundle.toString());
    }

    @Override
    public void onProviderEnabled(String s) {
        Log.d("Location Manager", "Provider enabled " + s);
    }

    @Override
    public void onProviderDisabled(String s) {
        Log.w("Location Manager", "Provider disabled " + s);
    }
}
