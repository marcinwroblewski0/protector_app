package pl.marcinwroblewski.protector;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

/**
 * Created by wrbl on 09.11.16.
 */

public class Animations {

    public static void imageViewAnimatedChange(Context c, final ImageView v, final int resource) {
        final Animation animOut = AnimationUtils.loadAnimation(c, android.R.anim.fade_out);
        final Animation animIn  = AnimationUtils.loadAnimation(c, android.R.anim.fade_in);
        animOut.setAnimationListener(new Animation.AnimationListener()
        {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation)
            {
                v.setImageResource(resource);
                animIn.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {}
                });
                v.startAnimation(animIn);
            }
        });
        v.startAnimation(animOut);
    }

    public static void textColorAnimatedChange(Context c, final TextView v, final int color, final String textAfter) {
        final Animation animOut = AnimationUtils.loadAnimation(c, android.R.anim.fade_out);
        final Animation animIn  = AnimationUtils.loadAnimation(c, android.R.anim.fade_in);
        animOut.setAnimationListener(new Animation.AnimationListener()
        {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation)
            {
                v.setTextColor(color);
                v.setText(textAfter);
                animIn.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {}
                });
                v.startAnimation(animIn);
            }
        });
        v.startAnimation(animOut);
    }

    public static void imageViewAnimatedChangeWithYoYo(final ImageView v, final int resource) {
        YoYo.with(Techniques.FlipOutX)
                .duration(300)
                .withListener(new Animator.AnimatorListener() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                    }
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        v.setImageResource(resource);
                        YoYo.with(Techniques.FlipInX)
                                .duration(400)
                                .playOn(v);
                    }
                    @Override
                    public void onAnimationCancel(Animator animation) {}
                    @Override
                    public void onAnimationRepeat(Animator animation) {}
                })
                .playOn(v);

    }

    public static void flyInViews(Context c, View... views) {
        for(int i = 0; i < views.length; i++) {
            Animation anim = AnimationUtils.loadAnimation(c, android.R.anim.fade_in);
            anim.setDuration(300 + (i * 50));
            anim.setStartOffset(i * 50);
            View view = views[i];
            view.startAnimation(anim);
        }
    }

    public static void slideOutView(Context c, View view) {
        Animation animOut = AnimationUtils.loadAnimation(c, android.R.anim.slide_out_right);
        view.startAnimation(animOut);
    }

    public static void slideInView(Context c, View view) {
        Animation animOut = AnimationUtils.loadAnimation(c, android.R.anim.slide_in_left);
        view.startAnimation(animOut);
    }

    public static void slideOutView(Context c, View view, Animation.AnimationListener listener) {
        Animation animOut = AnimationUtils.loadAnimation(c, android.R.anim.slide_out_right);
        animOut.setAnimationListener(listener);
        view.startAnimation(animOut);
    }

    public static void fadeOutView(Context c, View view) {
        final Animation animOut = AnimationUtils.loadAnimation(c, android.R.anim.fade_out);
        view.startAnimation(animOut);
    }
}
