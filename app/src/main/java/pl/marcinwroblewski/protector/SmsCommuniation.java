package pl.marcinwroblewski.protector;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import pl.marcinwroblewski.protector.guardian.Guardian;
import pl.marcinwroblewski.protector.guardian.addingPupil.AddingNewPupilState;
import pl.marcinwroblewski.protector.guardian.addingPupil.PasswordsForPupils;
import pl.marcinwroblewski.protector.guardian.addingPupil.PupilAddingOperationsList;
import pl.marcinwroblewski.protector.guardian.requests.SendingRequests;
import pl.marcinwroblewski.protector.pupil.Pupil;
import pl.marcinwroblewski.protector.pupil.addingGuardian.AddingNewGuardianState;
import pl.marcinwroblewski.protector.pupil.addingGuardian.GuardianAddingOperationsList;
import pl.marcinwroblewski.protector.pupil.addingGuardian.GuardiansPasswords;

/**
 * Created by Marcin Wróblewski on 16.01.2017.
 */

public class SmsCommuniation {

    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String ID = "id";
    public static final String COMMUNICATION_CODE = "communicationCode";
    public static final String CONTEXT = "context";
    public static final String DATA = "data";
    public static final String ENTRY_FORMULA = "This is a Protector configuration message: ";

    public static class ForGuardian {

        public static OperationResult askPupilForAdding(Pupil pupil, Context context) {

            try {
                AddingNewPupilState addingNewPupilState = new AddingNewPupilState(
                        pupil, AddingNewPupilState.ASKED_PUPIL_FOR_ADDING);
                String message = SmsCommuniation.createCommunicationSMS(
                        addingNewPupilState.getOperationState(), AddingNewPupilState.CONTEXT, context
                );
                SMSSender.sendSMSTo(pupil.getPhoneNumber(), message);

                addingNewPupilState = new AddingNewPupilState(
                        pupil, AddingNewPupilState.WAITING_FOR_PUPIL_RESPONSE_ABOUT_ADDING);
                PupilAddingOperationsList.addNewOperation(addingNewPupilState, context);

                return new OperationResult(OperationResult.STATUS_SUCCESS,
                        "askPupilForAdding method done");
            } catch (JSONException e) {
                e.printStackTrace();
                return new OperationResult(OperationResult.STATUS_ERROR,
                        "Cannot create SMS message");
            }
        }

        public static OperationResult pupilRespondedRequest(Pupil pupil, int pupilResponse, Context context) {

            Log.d("Pupil response", pupil.getFirstName() + ", code " + pupilResponse);

            AddingNewPupilState addingNewPupilState = PupilAddingOperationsList.getOperationFor(pupil, context);
            if(addingNewPupilState == null) {
                try {
                    return new OperationResult(
                            OperationResult.STATUS_ERROR,
                            "There's no operation for pupil: " + pupil.toJSONObject().toString(4));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            switch (pupilResponse) {
                case AddingNewPupilState.PUPIL_REJECTED:
                    PupilAddingOperationsList.modifyOperation(pupil, AddingNewPupilState.ADDING_REJECTED, context);
                    return new OperationResult(
                            OperationResult.STATUS_ERROR,
                            "Pupil rejected request"
                    );
                case AddingNewPupilState.PUPIL_ACCEPTED:
                    PupilAddingOperationsList.modifyOperation(pupil, AddingNewPupilState.PUPIL_ACCEPTED, context);
                    sendPupilPassword(pupil, context);
                    return new OperationResult(OperationResult.STATUS_SUCCESS,
                            "Password sent to pupil");
            }

            return new OperationResult(
                    OperationResult.STATUS_ERROR,
                    "Very strange app behavior"
            );
        }

        public static OperationResult sendPupilPassword(Pupil pupil, Context context) {

            try {
                AddingNewPupilState addingNewPupilState = new AddingNewPupilState(
                        pupil, AddingNewPupilState.PASSWORD_SENT_TO_PUPIL);


                JSONObject data = new JSONObject();
                data.put("password", PasswordsForPupils.getPasswordFor(pupil, context));

                String message = SmsCommuniation.createCommunicationSMS(
                        addingNewPupilState.getOperationState(),
                        AddingNewPupilState.CONTEXT,
                        data, context
                );
                SMSSender.sendSMSTo(pupil.getPhoneNumber(), message);
                PupilAddingOperationsList.modifyOperation(
                        addingNewPupilState.getAskedPupil(),
                        addingNewPupilState.getOperationState(),
                        context);

                Log.d("SMS", "SMS with password sent successfully");

                return new OperationResult(OperationResult.STATUS_SUCCESS,
                        "SMS with password sent successfully");
            } catch (JSONException e) {
                e.printStackTrace();
                return new OperationResult(OperationResult.STATUS_ERROR,
                        "Cannot create SMS message");
            }
        }

        public static OperationResult pupilConfirmedAddingGuardian(Pupil pupil, int pupilResponse, Context context) {

            AddingNewPupilState addingNewPupilState = PupilAddingOperationsList.getOperationFor(pupil, context);
            if(addingNewPupilState == null) {
                try {
                    return new OperationResult(
                            OperationResult.STATUS_ERROR,
                            "There's no operation for pupil: " + pupil.toJSONObject().toString(4));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            switch (pupilResponse) {
                case AddingNewPupilState.PUPIL_REJECTED_PASSWORD:
                    PupilAddingOperationsList.modifyOperation(pupil, AddingNewPupilState.ADDING_REJECTED, context);
                    return new OperationResult(
                            OperationResult.STATUS_ERROR,
                            "Pupil rejected password"
                    );
                case AddingNewPupilState.PUPIL_ACCEPTED_PASSWORD:
                    sendPupilPassword(pupil, context);
                    PupilAddingOperationsList.modifyOperation(pupil, AddingNewPupilState.ADDING_DONE, context);
                    return new OperationResult(OperationResult.STATUS_SUCCESS,
                            "Pupil added guardian");
            }

            return new OperationResult(
                    OperationResult.STATUS_ERROR,
                    "Very strange app behavior"
            );
        }

        public static OperationResult pupilAddingDone(Pupil pupil, Context context) {

            AddingNewPupilState addingNewPupilState = PupilAddingOperationsList.getOperationFor(pupil, context);
            if(addingNewPupilState == null) {
                try {
                    return new OperationResult(
                            OperationResult.STATUS_ERROR,
                            "There's no operation for pupil: " + pupil.toJSONObject().toString(4));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            PupilAddingOperationsList.modifyOperation(pupil, AddingNewPupilState.ADDING_DONE, context);
            return new OperationResult(OperationResult.STATUS_SUCCESS,
                    "Pupil added guardian");
        }


        public static OperationResult sendLocationRequest(Pupil pupil, Context context) {

            AddingNewPupilState addingNewPupilState = PupilAddingOperationsList.getOperationFor(pupil, context);
            if(addingNewPupilState == null) {
                try {
                    return new OperationResult(
                            OperationResult.STATUS_ERROR,
                            "There's no operation for pupil: " + pupil.toJSONObject().toString(4));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            try {
                SMSSender.sendSMSTo(pupil.getPhoneNumber(), createRequestSMS(
                        PasswordsForPupils.getPasswordFor(pupil, context),
                        SendingRequests.LOCATION_REQUEST_CODE, context));
                //TODO list of requests?
            } catch (JSONException e) {
                return new OperationResult(
                        OperationResult.STATUS_ERROR,
                        "Location request SMS wasn't sent: " + e.getMessage());
            }

            return new OperationResult(
                    OperationResult.STATUS_SUCCESS,
                    "Location request SMS was sent");
        }
    }











    public static class ForPupil {

        public static OperationResult addingGuardianRequest (Guardian guardian, Context context) {

            AddingNewGuardianState addingNewGuardianState = new AddingNewGuardianState(
                    guardian, AddingNewGuardianState.ADDING_REQUEST);

            //TODO ask user weather reject or accept

            GuardianAddingOperationsList.addNewOperation(addingNewGuardianState, context);

            return new OperationResult(OperationResult.STATUS_SUCCESS,
                    "Adding guardian request incoming");
        }

        public static OperationResult sendRequestResponse(Guardian guardian, int response, Context context) {

            AddingNewGuardianState addingNewGuardianState = GuardianAddingOperationsList.getOperationFor(guardian, context);
            if(addingNewGuardianState == null) {
                try {
                    GuardianAddingOperationsList.modifyOperation(guardian, AddingNewGuardianState.ADDING_REJECTED, context);
                    return new OperationResult(
                            OperationResult.STATUS_ERROR,
                            "There's no operation for guardian: " + guardian.toJSONObject().toString(4));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            try {
                String message = SmsCommuniation.createCommunicationSMS(response, AddingNewGuardianState.CONTEXT, context);
                SMSSender.sendSMSTo(guardian.getPhoneNumber(), message);
                if(response == AddingNewGuardianState.ACCEPT_REQUEST)
                    GuardianAddingOperationsList.modifyOperation(guardian, AddingNewGuardianState.WAITING_FOR_PASSWORD_FROM_GUARDIAN, context);
                else if (response == AddingNewGuardianState.REJECT_REQUEST)
                    GuardianAddingOperationsList.modifyOperation(guardian, AddingNewGuardianState.ADDING_REJECTED, context);

                return new OperationResult(
                        OperationResult.STATUS_SUCCESS,
                        "Response sent"
                );
            } catch (JSONException e) {
                e.printStackTrace();
                return new OperationResult(
                        OperationResult.STATUS_ERROR,
                        "Cannot create message for guardian");
            }


        }

        public static OperationResult receivePasswordFromGuardian(Guardian guardian, String password, Context context) {

            if(GuardianAddingOperationsList.getOperationFor(guardian, context) == null) {
                sendPasswordRejection(guardian, context);
                try {
                    return new OperationResult(
                            OperationResult.STATUS_ERROR,
                            "There's no operation for guardian: " + guardian.toJSONObject().toString(4)
                    );
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            AddingNewGuardianState addingNewGuardianState = new AddingNewGuardianState(
                    guardian, AddingNewGuardianState.PASSWORD_RECEIVED);
            GuardianAddingOperationsList.modifyOperation(guardian, AddingNewGuardianState.PASSWORD_RECEIVED, context);
            GuardiansPasswords.assign(guardian, password, context);

            sendAddingConfirmation(guardian, context);

            return new OperationResult(OperationResult.STATUS_SUCCESS,
                    "Password received and saved successfully");
        }

        public static OperationResult sendAddingConfirmation(Guardian guardian, Context context) {

            AddingNewGuardianState addingNewGuardianState = GuardianAddingOperationsList.getOperationFor(guardian, context);
            if(addingNewGuardianState == null) {
                try {
                    return new OperationResult(
                            OperationResult.STATUS_ERROR,
                            "There's no operation for guardian: " + guardian.toJSONObject().toString(4));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            try {
                String message = SmsCommuniation.createCommunicationSMS(AddingNewGuardianState.ADDING_DONE,
                        AddingNewGuardianState.CONTEXT, context);
                SMSSender.sendSMSTo(guardian.getPhoneNumber(), message);
                GuardianAddingOperationsList.modifyOperation(guardian, AddingNewGuardianState.ADDING_DONE, context);
                return new OperationResult(
                        OperationResult.STATUS_SUCCESS,
                        "Confirmation sent"
                );
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return new OperationResult(
                    OperationResult.STATUS_ERROR,
                    "Very strange app behavior"
            );
        }


        public static OperationResult sendPasswordRejection(Guardian guardian, Context context) {
            try {
                String message = SmsCommuniation.createCommunicationSMS(AddingNewGuardianState.REJECT_PASSWORD,
                        AddingNewGuardianState.CONTEXT, context);
                SMSSender.sendSMSTo(guardian.getPhoneNumber(), message);
                GuardianAddingOperationsList.modifyOperation(guardian, AddingNewGuardianState.ADDING_REJECTED, context);
                return new OperationResult(
                        OperationResult.STATUS_SUCCESS,
                        "Password rejection sent"
                );
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return new OperationResult(
                    OperationResult.STATUS_ERROR,
                    "Very strange app behavior"
            );
        }


        public static OperationResult handleLocationRequest(Guardian guardian, String password, Context context) {

            AddingNewGuardianState addingNewGuardianState = GuardianAddingOperationsList.getOperationFor(guardian, context);
            if(addingNewGuardianState == null) {
                try {
                    return new OperationResult(
                            OperationResult.STATUS_ERROR,
                            "There's no operation for guardian: " + guardian.toJSONObject().toString(4));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            if(!GuardiansPasswords.comparePasswords(guardian, password, context)) {
                return new OperationResult(
                        OperationResult.STATUS_ERROR,
                        "Password does not match");
            }

            AlertManager manager = new AlertManager((Application) context.getApplicationContext(), guardian);

            return new OperationResult(
                    OperationResult.STATUS_SUCCESS,
                    "Locating started");
        }
    }

    public static String createCommunicationSMS(int communicationCode, String context, Context app) throws JSONException {

        JSONObject object = new JSONObject();
        Identity userId = Identity.getFromPreferences(app);

        if(context.equals(AddingNewGuardianState.CONTEXT)) {
            object.put(ID, new Pupil(userId.getPhoneNumber(), userId.getFirstName(), userId.getLastName()).toJSONObject());
        } else if (context.equals(AddingNewPupilState.CONTEXT)) {
            object.put(ID, new Guardian(userId.getPhoneNumber(), userId.getFirstName(), userId.getLastName()).toJSONObject());
        }

        object.put(COMMUNICATION_CODE, communicationCode);
        object.put(CONTEXT, context);

        return ENTRY_FORMULA + object.toString();
    }

    public static String createCommunicationSMS(int communicationCode, String context, JSONObject data, Context app) throws JSONException {
        JSONObject object = new JSONObject();
        Identity userId = Identity.getFromPreferences(app);

        if(context.equals(AddingNewGuardianState.CONTEXT)) {
            object.put(ID, new Pupil(userId.getPhoneNumber(), userId.getFirstName(), userId.getLastName()).toJSONObject());
        } else if (context.equals(AddingNewPupilState.CONTEXT)) {
            object.put(ID, new Guardian(userId.getPhoneNumber(), userId.getFirstName(), userId.getLastName()).toJSONObject());
        }

        object.put(COMMUNICATION_CODE, communicationCode);
        object.put(DATA, data);
        object.put(CONTEXT, context);

        return ENTRY_FORMULA + object.toString();
    }

    public static String createRequestSMS(String password, int requestCode, Context context) throws JSONException {
        JSONObject object = new JSONObject();
        Identity userId = Identity.getFromPreferences(context);

        object.put(ID, new Guardian(userId.getPhoneNumber(), userId.getFirstName(), userId.getLastName()).toJSONObject());
        object.put(COMMUNICATION_CODE, requestCode);
        object.put(CONTEXT, AddingNewPupilState.CONTEXT);

        JSONObject data = new JSONObject();
        data.put("password", password);
        object.put(DATA, data);

        return ENTRY_FORMULA + object.toString();
    }
}
