package pl.marcinwroblewski.protector;

import android.app.Application;
import android.media.MediaPlayer;

import com.facebook.stetho.Stetho;

import pl.marcinwroblewski.protector.pupil.Pupil;

/**
 * Created by Admin on 02.12.2016.
 */

public class ProtectorApplication extends Application {

    private MediaPlayer mediaPlayer;
    private int usingDangerDetectionMode;
    private boolean collectingData;
    private boolean dangerDetectionTurnedOn;
    private Identity userInfo;
    private Pupil pupilThatBeingRequested;

    @Override
    public void onCreate() {
        super.onCreate();
        //todo test if guardian mode enabled and get guardian info

        Stetho.initializeWithDefaults(getApplicationContext());
        userInfo = Identity.getFromPreferences(getApplicationContext());

    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public void turnOnAlarmSound() {
        mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.alarm);
        mediaPlayer.setLooping(true);
        mediaPlayer.start();
    }

    public void turnOffAlarmSound() {
        if(mediaPlayer == null) return;

        mediaPlayer.stop();
    }

    public boolean isCollectingData() {
        return collectingData;
    }

    public void setCollectingData(boolean collectingData) {
        this.collectingData = collectingData;
    }

    public boolean isDangerDetectionTurnedOn() {
        return dangerDetectionTurnedOn;
    }

    public void setDangerDetectionTurnedOn(boolean dangerDetectionTurnedOn) {
        this.dangerDetectionTurnedOn = dangerDetectionTurnedOn;
    }

    public int getUsingDangerDetectionMode() {
        return usingDangerDetectionMode;
    }

    public void setUsingDangerDetectionMode(int usingDangerDetectionMode) {
        this.usingDangerDetectionMode = usingDangerDetectionMode;
    }

    public Identity getIdentity() {
        if(userInfo == null)
            userInfo = Identity.getFromPreferences(getApplicationContext());

        return userInfo;
    }

    public Pupil getPupilThatBeingRequested() {
        return pupilThatBeingRequested;
    }

    public void setPupilThatBeingRequested(Pupil pupilThatBeingRequested) {
        this.pupilThatBeingRequested = pupilThatBeingRequested;
    }
}
