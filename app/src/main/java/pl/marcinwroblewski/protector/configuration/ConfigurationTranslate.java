package pl.marcinwroblewski.protector.configuration;

import android.content.SharedPreferences;

/**
 * Created by wrbl on 15.11.16.
 */

public class ConfigurationTranslate {

    public static void setupConfigurationBySensitivenessLevel(int sensitivenessLevel, SharedPreferences.Editor editor) {

        switch (sensitivenessLevel) {
            case 0:
                editor.putInt("sensivity", 40);
                editor.putInt("analysisTime", 60);
                editor.putInt("dangerPoints", 18);
                break;
            case 1:
                editor.putInt("sensivity", 25);
                editor.putInt("analysisTime", 60);
                editor.putInt("dangerPoints", 10);
                break;
            case 2:
                editor.putInt("sensivity", 15);
                editor.putInt("analysisTime", 60);
                editor.putInt("dangerPoints", 6);
                break;
        }

        editor.apply();
    }

}
