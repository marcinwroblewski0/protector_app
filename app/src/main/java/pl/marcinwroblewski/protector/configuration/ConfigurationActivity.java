package pl.marcinwroblewski.protector.configuration;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import pl.marcinwroblewski.protector.MainActivity;
import pl.marcinwroblewski.protector.R;
import pl.marcinwroblewski.protector.configuration.Fragments.ConfigurationIntroduceFragment;
import pl.marcinwroblewski.protector.configuration.Fragments.CountdownTimeFragment;
import pl.marcinwroblewski.protector.configuration.Fragments.CreateIdentityFragment;
import pl.marcinwroblewski.protector.configuration.Fragments.EmergencyMessageConfigurationFragment;
import pl.marcinwroblewski.protector.configuration.Fragments.GPSPermissionAskFragment;
import pl.marcinwroblewski.protector.configuration.Fragments.PhoneNumberConfigurationFragment;
import pl.marcinwroblewski.protector.configuration.Fragments.SMSPermissionAskFragment;
import pl.marcinwroblewski.protector.configuration.Fragments.SensitivenessConfigurationFragment;
import pl.marcinwroblewski.protector.view.NonSwipeableViewPager;

public class ConfigurationActivity extends AppCompatActivity
        implements GPSPermissionAskFragment.GPSPermissionGrantedListener,
        SMSPermissionAskFragment.SMSPermissionGrantedListener,
        PhoneNumberConfigurationFragment.OnPhoneNumberProvidedListener,
        EmergencyMessageConfigurationFragment.OnEmergencyMessageProvidedListener,
        SensitivenessConfigurationFragment.OnSensivivenessPickedListener,
        CountdownTimeFragment.OnCountdownTimePickedListener,
        CreateIdentityFragment.IdentityCreatedListener,
        ConfigurationIntroduceFragment.OnIntroduceFinishedListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;
    SharedPreferences preferences;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private NonSwipeableViewPager mViewPager;
    private int currentPage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = getSharedPreferences(getPackageName(), MODE_PRIVATE);
        if(preferences.getBoolean("creatorDone", false)) {
            if(!getIntent().getBooleanExtra("reconfigure", false)) {
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
                finish();
            }
        }

        setContentView(R.layout.activity_configuration);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Protector configuration");
        toolbar.setLogo(R.mipmap.ic_launcher);
        setSupportActionBar(toolbar);
        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (NonSwipeableViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

    }

    @Override
    public void permissionGranted(View view) {
        moveToTheNextPage();
    }

    private void moveToTheNextPage() {
        currentPage++;
        mViewPager.setCurrentItem(currentPage, true);
    }

    private void moveToThePreviousPage() {
        if(currentPage <= 0) {
            super.onBackPressed();
            return;
        }

        currentPage--;
        mViewPager.setCurrentItem(currentPage, true);

        Log.d("Current page", "Move back: " + currentPage);
    }

    @Override
    public void onBackPressed() {
        moveToThePreviousPage();
    }

    @Override
    public void proceedToMessageCreator() {
        moveToTheNextPage();
    }

    @Override
    public void finishCreator() {
        startActivity(new Intent(getApplicationContext(), MainActivity.class));

        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean("creatorDone", true);
        editor.apply();

        finish();
    }

    @Override
    public void proceedToNextStep() {
        moveToTheNextPage();
    }

    @Override
    public void next() {
        currentPage++;
        mViewPager.setCurrentItem(currentPage, true);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            switch (position) {
                case 0:
                    return new ConfigurationIntroduceFragment();
                case 1:
                    return new GPSPermissionAskFragment();
                case 2:
                    return new SMSPermissionAskFragment();
                case 3:
                    return new SensitivenessConfigurationFragment();
                case 4:
                    return new PhoneNumberConfigurationFragment();
                case 5:
                    return new CountdownTimeFragment();
                case 6:
                    return new EmergencyMessageConfigurationFragment();
                case 7:
                    return new CreateIdentityFragment();
            }

            return null;
        }

        @Override
        public int getCount() {
            return 8;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return null;
        }
    }
}
