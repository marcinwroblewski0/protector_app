package pl.marcinwroblewski.protector.configuration.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pl.marcinwroblewski.protector.R;
import pl.marcinwroblewski.protector.configuration.ConfigurationFields;
import pl.marcinwroblewski.protector.view.ClickNumberPickerView;
import pl.polak.clicknumberpicker.ClickNumberPickerListener;
import pl.polak.clicknumberpicker.PickerClickType;

/**
 * A simple {@link Fragment} subclass.
 */
public class CountdownTimeFragment extends Fragment {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private View mainView;
    private ClickNumberPickerView countdownView;
    private AppCompatButton nextStepButton;
    private OnCountdownTimePickedListener onCountdownTimePickedListener;

    public CountdownTimeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.countdown_time_fragment, null);

        preferences = getContext().getSharedPreferences(getContext().getPackageName(), Context.MODE_PRIVATE);
        editor = preferences.edit();

        setupCountdownView();
        setupNextStepButton();
        return mainView;
    }

    private void setupCountdownView() {
        countdownView = (ClickNumberPickerView) mainView.findViewById(R.id.countdown);
        countdownView.setPickerValue(preferences.getInt(ConfigurationFields.COUNTDOWN_TIME, 50));

        countdownView.setClickNumberPickerListener(new ClickNumberPickerListener() {
            @Override
            public void onValueChange(float previousValue, float currentValue, PickerClickType pickerClickType) {
                editor.putInt(ConfigurationFields.COUNTDOWN_TIME, (int)currentValue);
            }
        });
    }

    private void setupNextStepButton() {
        nextStepButton = (AppCompatButton) mainView.findViewById(R.id.ready_button);
        nextStepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                editor.apply();
                onCountdownTimePickedListener.proceedToMessageCreator();
            }
        });
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            if(context instanceof Activity)
                onCountdownTimePickedListener = (OnCountdownTimePickedListener) context;
            else
                throw new Exception(context.toString() + " is not instanceof Activity");

        } catch (Exception e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnPhoneNumberProvidedListener");
        }
    }


    public interface OnCountdownTimePickedListener {
        void proceedToMessageCreator();
    }
}
