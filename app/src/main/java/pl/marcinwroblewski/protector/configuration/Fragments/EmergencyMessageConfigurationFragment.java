package pl.marcinwroblewski.protector.configuration.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import pl.marcinwroblewski.protector.R;
import pl.marcinwroblewski.protector.configuration.ConfigurationFields;

/**
 * A simple {@link Fragment} subclass.
 */
public class EmergencyMessageConfigurationFragment extends Fragment {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private View mainView;
    private EditText emergencyMessageET;
    private AppCompatButton nextStepButton;
    private OnEmergencyMessageProvidedListener onEmergencyMessageProvidedListener;

    public EmergencyMessageConfigurationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_emergency_text_configuration, null);

        preferences = getContext().getSharedPreferences(getContext().getPackageName(), Context.MODE_PRIVATE);
        editor = preferences.edit();

        setupEditTexts();
        setupNextStepButton();

        return mainView;
    }


    private void setupEditTexts() {
        emergencyMessageET = (EditText) mainView.findViewById(R.id.emergency_text);
        if(preferences.contains(ConfigurationFields.EMERGENCY_MESSAGE)) {
            emergencyMessageET.setText(preferences.getString(ConfigurationFields.EMERGENCY_MESSAGE, ""));
        }
    }

    private void setupNextStepButton() {
        nextStepButton = (AppCompatButton) mainView.findViewById(R.id.ready_button);
        nextStepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyData();
            }
        });
    }

    private void verifyData() {
        if(emergencyMessageET.getText().length() < 10) {
            emergencyMessageET.setError("Please provide your emergency message");
            return;
        }

        editor.putString(ConfigurationFields.EMERGENCY_MESSAGE, emergencyMessageET.getText().toString());
        editor.apply();
        onEmergencyMessageProvidedListener.proceedToNextStep();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            if(context instanceof Activity)
                onEmergencyMessageProvidedListener = (OnEmergencyMessageProvidedListener) context;
            else
                throw new Exception(context.toString() + " is not instanceof Activity");

        } catch (Exception e) {
            throw new ClassCastException(context.toString()
                    + " must implement SMSPermissionGrantedListener");
        }
    }


    public interface OnEmergencyMessageProvidedListener {
        public void proceedToNextStep();
    }
}
