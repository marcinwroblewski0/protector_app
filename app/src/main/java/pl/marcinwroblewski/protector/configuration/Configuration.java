package pl.marcinwroblewski.protector.configuration;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Marcin Wróblewski on 28.11.2016.
 */

public class Configuration {

    private SharedPreferences preferences;
    private Context context;


    public Configuration(Context context) {
        this.context = context;
        preferences = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
    }

    public int getTriggeringAcceleretion() {
        return preferences.getInt("acceleration", 25);
    }

    public int getMaxDangerPoints() {
        return preferences.getInt("dangerPoints", 15);
    }

    public boolean usingOnlyAccelerometer() {
        return preferences.getBoolean("accelerometerOnly", false);
    }

    public int getAnalysisTime() {
        return preferences.getInt("analysisTime", 60);
    }

    public String getPhoneNumber() {
        return preferences.getString(ConfigurationFields.PHONE_COUNTRY_CODE, "") + preferences.getString(ConfigurationFields.PHONE_NUMBER, "");
    }

    public String getEmergencyMessage() {
        return preferences.getString(ConfigurationFields.EMERGENCY_MESSAGE, "");
    }

    public int getCountdownTime() {
        return preferences.getInt(ConfigurationFields.COUNTDOWN_TIME, 50);
    }

}
