package pl.marcinwroblewski.protector.configuration.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatSpinner;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import java.util.ArrayList;

import pl.marcinwroblewski.protector.R;
import pl.marcinwroblewski.protector.SensitivenessSpinnerAdapter;
import pl.marcinwroblewski.protector.configuration.ConfigurationFields;
import pl.marcinwroblewski.protector.configuration.ConfigurationTranslate;

/**
 * A simple {@link Fragment} subclass.
 */
public class SensitivenessConfigurationFragment extends Fragment {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private View mainView;
    private AppCompatSpinner spinner;
    private AppCompatButton nextStepButton;
    private OnSensivivenessPickedListener fragmentListener;
    private int optionSelected;

    public SensitivenessConfigurationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_sensitiveness_configuration, null);

        preferences = getContext().getSharedPreferences(getContext().getPackageName(), Context.MODE_PRIVATE);
        editor = preferences.edit();

        setupSpinner();
        setupNextStepButton();
        return mainView;
    }

    private void setupSpinner() {
        spinner = (AppCompatSpinner) mainView.findViewById(R.id.sensitiveness_spinner);

        ArrayList<Integer> images = new ArrayList<>();
        images.add(R.drawable.car);
        images.add(R.drawable.stairs);
        images.add(R.drawable.leaf);

        ArrayList<String> names = new ArrayList<>();
        names.add("Hit by a car");
        names.add("Fall down the stairs");
        names.add("\"Hit\" by a leaf");

        SensitivenessSpinnerAdapter adapter = new SensitivenessSpinnerAdapter(images, names, getContext());
        spinner.setAdapter(adapter);

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                optionSelected = i;
                Log.d("Spinner", i + ", " + l);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                optionSelected = 0;
            }
        });
    }

    private void setupNextStepButton() {
        nextStepButton = (AppCompatButton) mainView.findViewById(R.id.ready_button);
        nextStepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                saveData(optionSelected);
            }
        });
    }

    private void saveData(int sensitivenessLevel) {
        editor.putInt(ConfigurationFields.SENSITIVENESS, sensitivenessLevel);

        ConfigurationTranslate.setupConfigurationBySensitivenessLevel(sensitivenessLevel, editor);

        editor.apply();
        fragmentListener.proceedToMessageCreator();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            if(context instanceof Activity)
                fragmentListener = (OnSensivivenessPickedListener) context;
            else
                throw new Exception(context.toString() + " is not instanceof Activity");

        } catch (Exception e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnSensivivenessPickedListener");
        }
    }


    public interface OnSensivivenessPickedListener {
        void proceedToMessageCreator();
    }
}
