package pl.marcinwroblewski.protector.configuration.Fragments;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import pl.marcinwroblewski.protector.Identity;
import pl.marcinwroblewski.protector.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class CreateIdentityFragment extends Fragment {

    private View mainView;
    private IdentityCreatedListener listener;
    private EditText phoneNumberET, firstNameET, lastNameET;

    public CreateIdentityFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_configuration_identity, null);
        setupViews();
        setupNextStepButton();
        return mainView;
    }


    private void setupViews() {
        phoneNumberET = (EditText) mainView.findViewById(R.id.phone_number);
        TelephonyManager telephonyManager = (TelephonyManager) getActivity().getSystemService(Context.TELEPHONY_SERVICE);
        String phoneNumber = telephonyManager.getLine1Number();

        if(phoneNumber != null && !phoneNumber.isEmpty() && !phoneNumber.contains("?")) {
            phoneNumberET.setText(phoneNumber);
        }

        firstNameET = (EditText) mainView.findViewById(R.id.first_name);
        lastNameET = (EditText) mainView.findViewById(R.id.last_name);
    }

    private boolean validate() {
        String phoneNumber = phoneNumberET.getText().toString();
        if(phoneNumber.length() <= 4) {
            phoneNumberET.setError("Phone number is too short");
            return false;
        }

        if(!Patterns.PHONE.matcher(phoneNumber).matches()) {
            phoneNumberET.setError("It's not valid phone number");
            return false;
        }

        String firstName = firstNameET.getText().toString();
        if(firstName == null || firstName.isEmpty()) {
            firstNameET.setError("Put your first name here");
            return false;
        }

        String lastName = lastNameET.getText().toString();
        if(lastName == null || lastName.isEmpty()) {
            lastNameET.setError("Put your first name here");
            return false;
        }

        return true;
    }

    private void setupNextStepButton() {
        Button nextStepButton = (Button) mainView.findViewById(R.id.ready_button);


        nextStepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(validate()) {
                    Identity identity = new Identity(
                            phoneNumberET.getText().toString(),
                            firstNameET.getText().toString(),
                            lastNameET.getText().toString()
                    );
                    Identity.saveToPreferences(identity, getContext());
                    listener.finishCreator();
                }
            }
        });
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            if(context instanceof Activity)
                listener = (IdentityCreatedListener) context;
            else
                throw new Exception(context.toString() + " is not instanceof Activity");

        } catch (Exception e) {
            throw new ClassCastException(context.toString()
                    + " must implement SMSPermissionGrantedListener");
        }
    }

    public interface IdentityCreatedListener {
        public void finishCreator();
    }

}
