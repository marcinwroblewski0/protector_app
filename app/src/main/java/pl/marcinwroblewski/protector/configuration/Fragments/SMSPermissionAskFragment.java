package pl.marcinwroblewski.protector.configuration.Fragments;


import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import pl.marcinwroblewski.protector.Animations;
import pl.marcinwroblewski.protector.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class SMSPermissionAskFragment extends Fragment {

    private View mainView;
    private static final int SMS_PERMISSION = 69;
    private boolean arePermissionGranted;
    private SMSPermissionGrantedListener smsPermissionGrantedListener;

    public SMSPermissionAskFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_sms_premission_ask, null);
        checkPermissions();
        return mainView;
    }

    private void checkPermissions() {
        if (
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.SEND_SMS) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.RECEIVE_SMS) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_SMS) != PackageManager.PERMISSION_GRANTED ||
                ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            arePermissionGranted = false;
        } else {
            arePermissionGranted = true;
        }
        Log.d("permission SMS", "Granted?: " + arePermissionGranted);
        setupViews();
    }

    private void askForPermissions() {
        requestPermissions(
                new String[]{Manifest.permission.SEND_SMS, Manifest.permission.RECEIVE_SMS, Manifest.permission.READ_SMS, Manifest.permission.READ_PHONE_STATE},
                SMS_PERMISSION);
    }

    private void setupViews() {
        setupStatusImage();
        setupNextStepButton();
        setupPermissionAskButton();
    }

    private void setupStatusImage() {
        ImageView permissionStatusIV = (ImageView) mainView.findViewById(R.id.permission_status_image);
        Animations.imageViewAnimatedChange(getContext(), permissionStatusIV,
                (arePermissionGranted) ? R.drawable.granted : R.drawable.rejected);
    }

    private void setupNextStepButton() {
        Button nextStepButton = (Button) mainView.findViewById(R.id.ready_button);
        nextStepButton.setActivated(arePermissionGranted);
        nextStepButton.setClickable(arePermissionGranted);
        if(!arePermissionGranted) {
            nextStepButton.setTextColor(getContext().getResources().getColor(R.color.disabled));
            nextStepButton.setOnClickListener(null);
        } else {
            nextStepButton.setTextColor(getContext().getResources().getColor(R.color.colorAccent));
            nextStepButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    smsPermissionGrantedListener.permissionGranted(mainView);
                }
            });
        }
    }

    private void setupPermissionAskButton() {
        Button askPermissionButton = (Button) mainView.findViewById(R.id.ask_permission_button);
        askPermissionButton.setVisibility((arePermissionGranted) ? View.GONE : View.VISIBLE);
        askPermissionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                askForPermissions();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case SMS_PERMISSION: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    arePermissionGranted = true;
                } else {
                    arePermissionGranted = false;
                }
                setupViews();
                Log.d("onPermission SMS", "Granted?: " + arePermissionGranted);
                return;
            }
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            if(context instanceof Activity)
                smsPermissionGrantedListener = (SMSPermissionGrantedListener) context;
            else
                throw new Exception(context.toString() + " is not instanceof Activity");

        } catch (Exception e) {
            throw new ClassCastException(context.toString()
                    + " must implement SMSPermissionGrantedListener");
        }
    }

    public interface SMSPermissionGrantedListener {
        public void permissionGranted(View view);
    }

}
