package pl.marcinwroblewski.protector.configuration.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import pl.marcinwroblewski.protector.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfigurationIntroduceFragment extends Fragment {


    OnIntroduceFinishedListener onIntroduceFinishedListener;
    View readyButton, privacyPolicyButton;

    public ConfigurationIntroduceFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View root  = inflater.inflate(R.layout.fragment_configuration_introduce, container, false);
        readyButton = root.findViewById(R.id.ready_button);
        readyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onIntroduceFinishedListener.next();
            }
        });
        privacyPolicyButton = root.findViewById(R.id.privacy_policy_button);
        privacyPolicyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://superprotector.eu/privacy"));
                startActivity(browserIntent);
            }
        });
        return root;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            if(context instanceof Activity)
                onIntroduceFinishedListener = (OnIntroduceFinishedListener) context;
            else
                throw new Exception(context.toString() + " is not instanceof Activity");

        } catch (Exception e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnPhoneNumberProvidedListener");
        }
    }


    public interface OnIntroduceFinishedListener {
        void next();
    }

}
