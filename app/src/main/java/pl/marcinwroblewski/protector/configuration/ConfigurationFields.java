package pl.marcinwroblewski.protector.configuration;

/**
 * Created by wrbl on 09.11.16.
 */

public class ConfigurationFields {

    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String PHONE_COUNTRY_CODE = "phoneCountryCode";
    public static final String EMERGENCY_MESSAGE = "emergencyMessage";
    public static final String SENSITIVENESS = "sensitiveness";
    public static final String COUNTDOWN_TIME = "countdownTime";
}
