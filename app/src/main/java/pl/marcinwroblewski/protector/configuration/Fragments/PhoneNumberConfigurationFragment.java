package pl.marcinwroblewski.protector.configuration.Fragments;


import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import pl.marcinwroblewski.protector.R;
import pl.marcinwroblewski.protector.configuration.ConfigurationFields;
import pl.marcinwroblewski.protector.guardian.Guardian;
import pl.marcinwroblewski.protector.pupil.addingGuardian.AddingNewGuardianState;
import pl.marcinwroblewski.protector.pupil.addingGuardian.GuardianAddingOperationsList;

/**
 * A simple {@link Fragment} subclass.
 */
public class PhoneNumberConfigurationFragment extends Fragment {

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private View mainView;
    private EditText countryCodeET, phoneNumberET;
    private AppCompatButton nextStepButton;
    private OnPhoneNumberProvidedListener onPhoneNumberProvidedListener;

    public PhoneNumberConfigurationFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mainView = inflater.inflate(R.layout.fragment_phone_number_configuration, null);

        preferences = getContext().getSharedPreferences(getContext().getPackageName(), Context.MODE_PRIVATE);
        editor = preferences.edit();

        setupEditTexts();
        setupNextStepButton();
        return mainView;
    }

    private void setupEditTexts() {
        countryCodeET = (EditText) mainView.findViewById(R.id.phone_country_code);

        if(preferences.contains(ConfigurationFields.PHONE_COUNTRY_CODE)) {
            countryCodeET.setText(preferences.getString(ConfigurationFields.PHONE_COUNTRY_CODE, ""));
        }
        countryCodeET.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if(keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    phoneNumberET.requestFocus();
                }
                return false;
            }
        });


        phoneNumberET = (EditText) mainView.findViewById(R.id.phone_number);
        if(preferences.contains(ConfigurationFields.PHONE_NUMBER)) {
            phoneNumberET.setText(preferences.getString(ConfigurationFields.PHONE_NUMBER, ""));
        }
        phoneNumberET.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if(keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
                    verifyData();
                }
                return false;
            }
        });


    }

    private void setupNextStepButton() {
        nextStepButton = (AppCompatButton) mainView.findViewById(R.id.ready_button);
        nextStepButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                verifyData();
            }
        });
    }

    private void verifyData() {
        if(countryCodeET.getText().length() < 2) {
            countryCodeET.setError("Please provide your country code");
            return;
        } //else {
//            if(!countryCodeET.getText().toString().contains("+")) {
//                String countryCode = "+" + countryCodeET.getText().toString();
//                countryCodeET.setText(countryCode);
//            }
//        }


        if(phoneNumberET.getText().length() < 7) {
            phoneNumberET.setError("Please provide proper phone number");
            return;
        }

        Guardian defaultGuardian = new Guardian(
                countryCodeET.getText().toString() + phoneNumberET.getText().toString(),
                "Default",
                ""
        );

        AddingNewGuardianState state = new AddingNewGuardianState(defaultGuardian, 200);

        GuardianAddingOperationsList.addNewOperation(state, getContext());
        onPhoneNumberProvidedListener.proceedToMessageCreator();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception
        try {
            if(context instanceof Activity)
                onPhoneNumberProvidedListener = (OnPhoneNumberProvidedListener) context;
            else
                throw new Exception(context.toString() + " is not instanceof Activity");

        } catch (Exception e) {
            throw new ClassCastException(context.toString()
                    + " must implement OnPhoneNumberProvidedListener");
        }
    }


    public interface OnPhoneNumberProvidedListener {
        void proceedToMessageCreator();
    }
}
