package pl.marcinwroblewski.protector;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * Created by Marcin Wróblewski on 29.05.16.
 */
public class NotificationDismissedReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("DismissReceiver", "Dissmissed");
        NotificationsManager.itsOk = true;
    }
}
