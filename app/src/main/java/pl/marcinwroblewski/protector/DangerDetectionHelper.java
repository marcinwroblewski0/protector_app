package pl.marcinwroblewski.protector;

import android.content.Context;

/**
 * Created by Marcin Wróblewski on 11.01.17.
 */

class DangerDetectionHelper {

    public static final int MODE_ACCELEROMETER = 100;
    public static final int MODE_ACCELEROMETER_AND_GYROSCOPE = 200;

    private int usingMode;
    private Context context;
    private ProtectorApplication application;

    public DangerDetectionHelper(Context context, int mode) {
        this.context = context;
        usingMode = mode;

        application = (ProtectorApplication) context.getApplicationContext();
        application.setUsingDangerDetectionMode(mode);
        application.setDangerDetectionTurnedOn(true);
    }

    public boolean detectDanger(float difference) {
        return DataAnalytics.isDangerPointsLevelDangerous((int) difference, context);
    }

    public OperationResult turnOnDangerDetection() {
        application.setDangerDetectionTurnedOn(true);
        return new OperationResult(
                OperationResult.STATUS_SUCCESS, "Application is now in DangerDetection mode"
        );
    }

    public OperationResult turnOffDangerDetection() {
        application.setDangerDetectionTurnedOn(false);
        startCollectingData();
        return new OperationResult(
                OperationResult.STATUS_SUCCESS, "DangerDetection mode is now turned off"
        );
    }

    public OperationResult startCollectingData() {

        application.setCollectingData(true);
        return new OperationResult(
                OperationResult.STATUS_SUCCESS, "Application is now in CollectingData mode"
        );

    }

    public OperationResult endCollectingData() {
        application.setCollectingData(false);
        turnOnDangerDetection();
        return new OperationResult(
                OperationResult.STATUS_SUCCESS, "CollectingData mode is now turned off"
        );
    }
}
