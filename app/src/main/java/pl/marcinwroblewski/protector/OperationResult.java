package pl.marcinwroblewski.protector;

import android.util.Log;

/**
 * Created by Marcin Wróblewski on 11.01.17.
 */

public class OperationResult {

    public static final String STATUS_ERROR = "error";
    public static final String STATUS_SUCCESS = "success";

    private String status, message;

    public OperationResult(String status, String message) {

        this.status = status;
        this.message = message;

        Log.d("OperationResult", toString());
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return getStatus() + ": " + getMessage();
    }
}
