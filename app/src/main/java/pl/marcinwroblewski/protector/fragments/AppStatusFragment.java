package pl.marcinwroblewski.protector.fragments;


import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatSeekBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import java.util.Random;

import pl.marcinwroblewski.protector.Animations;
import pl.marcinwroblewski.protector.DangerService;
import pl.marcinwroblewski.protector.LocationServicesCheck;
import pl.marcinwroblewski.protector.NotificationsManager;
import pl.marcinwroblewski.protector.R;
import pl.marcinwroblewski.protector.SensorsList;
import pl.marcinwroblewski.protector.view.CardGenerator;

import static android.content.Context.MODE_PRIVATE;
import static android.content.Context.SENSOR_SERVICE;

/**
 * A simple {@link Fragment} subclass.
 */
public class AppStatusFragment extends Fragment implements SensorEventListener {

    private LinearLayout mainContainer;
    private AppCompatSeekBar sensivitySeekbar, protectivenessSeekbar, accuracySeekbar;
    private TextView debug, gyroscopeDebug;
    private TextView status;
    private ImageView statusImage;
    private AppCompatCheckBox logsCheckbox;
    private AppCompatButton serviceStatusChange;
    private float[] lastSensorValue;
    private int maxX, maxY, maxZ;
    private SensorsList sensorsList;
    private SensorManager sensorManager;
    private static final int REQUESTED_PERMISSIONS = 1;
    private View fakeAlarmButton;

    String[] working = {"Working", "Detecting", "Running", "Awake", "Open-eyed", "Watchful",
            "Vigilant", "Wide-awake"};
    String[] sleep = {"Sleeping", "Chillin'", "Pensive"};


    public AppStatusFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_app_status, container, false);
        mainContainer = (LinearLayout) rootView.findViewById(R.id.container);


        sensorManager = (SensorManager) getContext().getSystemService(SENSOR_SERVICE);
        List<Sensor> sensors = sensorManager.getSensorList(Sensor.TYPE_ALL);
        if (debug != null) {
            debug.setText("Available (" + String.valueOf(sensors.size()) + ")");
        }

        setupSensorsList(sensors);

        sensorsList = new SensorsList(getContext());
        Sensor accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        Sensor rotation = sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);


        if(!getContext().getSharedPreferences(getContext().getPackageName(), MODE_PRIVATE).getBoolean("accelerometerOnly", false) &&
                sensorManager.registerListener(this, rotation, SensorManager.SENSOR_DELAY_NORMAL)) {
            sensorsList.addSensor(Sensor.TYPE_GRAVITY, rotation.getName());
            setupGyroscopeDebug();
            gyroscopeDebug.setText(rotation.getName());
        } else {
            Toast.makeText(getContext(),
                    "Your device is not fully supported due to lack of gyroscope",
                    Toast.LENGTH_LONG)
                    .show();
            getContext().getSharedPreferences(getContext().getPackageName(), MODE_PRIVATE)
                    .edit()
                    .putBoolean("accelerometerOnly", true)
                    .apply();
        }

        if(sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL)) {
            setupSensivityChooser(accelerometer);
            sensorsList.addSensor(Sensor.TYPE_ACCELEROMETER, accelerometer.getName());
        } else {
            Toast.makeText(getContext(),
                    "Your device is not supported due to lack of accelerometer",
                    Toast.LENGTH_LONG)
                    .show();
        }


        setupDebug();
        setupGyroscopeDebug();
        setupStatus();
        setupServiceChange();
        setupDeveloperOptions();
        setupProtectivenessChooser();
        setupAccuracyChooser();
        setupFakeAlarmButton();

        return rootView;
    }


    private void setupFakeAlarmButton() {
        fakeAlarmButton = mainContainer.findViewById(R.id.fake_alarm);
        fakeAlarmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NotificationsManager.showDangerNotification(getActivity().getApplication());
            }
        });
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if(!getContext().getSharedPreferences(getContext().getPackageName(), MODE_PRIVATE).getBoolean("developer", false)) {
            sensorManager.unregisterListener(this);
            return;
        } else {

            String eventSensorName = event.sensor.getName();

            if (eventSensorName.equals(sensorsList.getSensorName(Sensor.TYPE_ACCELEROMETER))) {

                lastSensorValue = event.values.clone();
                if (maxX < lastSensorValue[0] || maxY < lastSensorValue[1] || maxZ < lastSensorValue[2]) {
                    if (maxX < lastSensorValue[0])
                        maxX = (int) lastSensorValue[0];

                    if (maxY < lastSensorValue[1])
                        maxY = (int) lastSensorValue[1];

                    if (maxZ < lastSensorValue[2])
                        maxZ = (int) lastSensorValue[2];


                }

                debug.setText(event.sensor.getName() +
                        " \nX:" + maxX + ", " + lastSensorValue[0] +
                        " \nY:" + maxY + ", " + lastSensorValue[1] +
                        " \nZ:" + maxZ + ", " + lastSensorValue[2]);
            } else if (eventSensorName.equals(sensorsList.getSensorName(Sensor.TYPE_GRAVITY))) {

                gyroscopeDebug.setText(event.sensor.getName() +
                        " \nX: " + event.values[0] +
                        " \nY: " + event.values[1] +
                        " \nZ: " + event.values[2]);


            } else {
                gyroscopeDebug.setText("Weird" + event.sensor.getName() +
                        " \nX: " + event.values[0] +
                        " \nY: " + event.values[1] +
                        " \nZ: " + event.values[2]);
            }


        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
        Log.d(sensor.getName(), "Accuracy changed: " + accuracy);
    }



    private void setupSensivityChooser(Sensor sensor) {
        sensivitySeekbar = (AppCompatSeekBar) mainContainer.findViewById(R.id.sensivity_seekbar);

        final SharedPreferences sp = getContext().getSharedPreferences(
                getContext().getPackageName(), MODE_PRIVATE
        );


        Log.d(sensor.getName(), "Max range: " + sensor.getMaximumRange() + ", set: " + sp.getInt("sensivity", -1));
        sensivitySeekbar.setMax(50);
        sensivitySeekbar.setProgress(sp.getInt("sensivity", 34));
        sensivitySeekbar.refreshDrawableState();

        sensivitySeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            SharedPreferences.Editor spEditor = sp.edit();

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                spEditor.putInt("sensivity", progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                spEditor.apply();
                Log.d("Settings", "Set " + sp.getInt("sensivity", -1));
            }
        });
    }

    private void setupProtectivenessChooser() {
        protectivenessSeekbar = (AppCompatSeekBar) mainContainer.findViewById(R.id.protectiveness_seekbar);

        final SharedPreferences sp = getContext().getSharedPreferences(
                getContext().getPackageName(), MODE_PRIVATE
        );

        protectivenessSeekbar.setMax(20);
        protectivenessSeekbar.setProgress(sp.getInt("dangerPoints", 15));
        protectivenessSeekbar.refreshDrawableState();

        protectivenessSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            SharedPreferences.Editor spEditor = sp.edit();

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                spEditor.putInt("dangerPoints", progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                spEditor.apply();
                Log.d("Settings", "Set " + sp.getInt("dangerPoints", -1));
            }
        });
    }

    private void setupAccuracyChooser() {
        accuracySeekbar = (AppCompatSeekBar) mainContainer.findViewById(R.id.accuracy_seekbar);

        final SharedPreferences sp = getContext().getSharedPreferences(
                getContext().getPackageName(), MODE_PRIVATE
        );

        accuracySeekbar.setProgress(sp.getInt("analysisTime", 67));
        accuracySeekbar.refreshDrawableState();

        accuracySeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            SharedPreferences.Editor spEditor = sp.edit();

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                spEditor.putInt("analysisTime", progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

                spEditor.apply();
                Log.d("Settings", "Set " + sp.getInt("analysisTime", -1));
            }
        });
    }

    private void setupDebug() {
        debug = (TextView) mainContainer.findViewById(R.id.title);
    }

    private void setupSensorsList(List<Sensor> sensors) {
        LinearLayout sensorsList = (LinearLayout) mainContainer.findViewById(R.id.container);

        if(sensorsList != null)
            for(Sensor sensor : sensors) {
                sensorsList.addView(CardGenerator.getCardView(sensor.getName(), sensor.getVendor(),
                        getContext()));
            }
    }

    private void setupStatus() {
        status = (TextView) mainContainer.findViewById(R.id.service_status);
        statusImage = (ImageView) mainContainer.findViewById(R.id.status_image);
        serviceStatusChange = (AppCompatButton) mainContainer.findViewById(R.id.change_service_status);

        Random random = new Random();

        assert status != null;
        if(isMyServiceRunning(DangerService.class)) {
            Animations.textColorAnimatedChange(
                    getContext(),
                    status,
                    getResources().getColor(R.color.status_launched),
                    working[random.nextInt(working.length)]);
            Animations.textColorAnimatedChange(
                    getContext(),
                    serviceStatusChange,
                    getResources().getColor(R.color.status_sleep),
                    "Stop");
            Animations.imageViewAnimatedChangeWithYoYo(statusImage, R.drawable.ic_security_on);


            if(!LocationServicesCheck.isLocationEnabled(getContext())) {
                LocationServicesCheck.showLocationWarning(getActivity());
            }

        } else {
            Animations.textColorAnimatedChange(
                    getContext(),
                    status,
                    getResources().getColor(R.color.status_sleep),
                    sleep[random.nextInt(sleep.length)]);
            Animations.textColorAnimatedChange(
                    getContext(),
                    serviceStatusChange,
                    getResources().getColor(R.color.status_launched),
                    "Start");

            Animations.imageViewAnimatedChange(getContext(), statusImage, R.drawable.ic_security_off);
        }


    }

    private void setupServiceChange() {
        serviceStatusChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isMyServiceRunning(DangerService.class)) {
                    stopService();
                } else {
                    getActivity().startService(new Intent(getContext(), DangerService.class));
                }

                setupStatus();
            }
        });
    }

    private void setupDeveloperOptions() {
        logsCheckbox = (AppCompatCheckBox) mainContainer.findViewById(R.id.logs_checkbox);

        SharedPreferences sp = getContext().getSharedPreferences(getContext().getPackageName(), MODE_PRIVATE);
        final SharedPreferences.Editor spEditor = getContext().getSharedPreferences(getContext().getPackageName(), MODE_PRIVATE).edit();

        logsCheckbox.setChecked(sp.getBoolean("logs", false));

        logsCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                spEditor.putBoolean("logs", isChecked);
                spEditor.apply();
            }
        });

        AppCompatCheckBox developerMode = (AppCompatCheckBox) mainContainer.findViewById(R.id.developer_mode_checkbox);

        assert developerMode != null;
        developerMode.setChecked(sp.getBoolean("developer", false));

        developerMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                spEditor.putBoolean("developer", isChecked);
                spEditor.apply();

                if(isChecked) {
                    Toast.makeText(getContext(), "Restart the app", Toast.LENGTH_SHORT).show();
                }
            }
        });

        AppCompatCheckBox accelerometerOnly = (AppCompatCheckBox) mainContainer.findViewById(R.id.only_accelerometer);

        assert accelerometerOnly != null;
        accelerometerOnly.setChecked(sp.getBoolean("accelerometerOnly", false));

        accelerometerOnly.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                spEditor.putBoolean("accelerometerOnly", isChecked);
                spEditor.apply();
            }
        });

    }

    private void setupGyroscopeDebug() {
        gyroscopeDebug = (TextView) mainContainer.findViewById(R.id.gyroscope_debug);
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getContext().getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    private void stopService() {
        getActivity().stopService(new Intent(getContext(), DangerService.class));
    }

    @Override
    public void onStop() {
        super.onStop();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        sensorManager.unregisterListener(this);
    }
}
