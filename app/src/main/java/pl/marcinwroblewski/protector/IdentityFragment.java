package pl.marcinwroblewski.protector;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Marcin Wróblewski on 22.01.2017.
 */

public class IdentityFragment extends Fragment {

    private Identity userIdentity;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ProtectorApplication application = (ProtectorApplication) getActivity().getApplication();
        userIdentity = application.getIdentity();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View mainView = inflater.inflate(R.layout.identity_fragment, null);

        ((TextView)mainView.findViewById(R.id.phone_number)).setText(userIdentity.getPhoneNumber());
        ((TextView)mainView.findViewById(R.id.first_name)).setText(userIdentity.getFirstName());
        ((TextView)mainView.findViewById(R.id.last_name)).setText(userIdentity.getLastName());

        return mainView;
    }
}
