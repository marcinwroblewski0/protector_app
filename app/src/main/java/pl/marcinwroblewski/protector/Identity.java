package pl.marcinwroblewski.protector;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

/**
 * Created by Marcin Wróblewski on 22.01.2017.
 */

public class Identity {

    public static final String PHONE_NUMBER = "phoneNumber";
    public static final String FIRST_NAME = "firstName";
    public static final String LAST_NAME = "lastName";

    private String phoneNumber, firstName, lastName;


    public Identity(String phoneNumber, String firstName, String lastName) {
        this.phoneNumber = phoneNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public static Identity getFromPreferences(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE
        );
        String phoneNumber, firstName, lastName;
        phoneNumber = preferences.getString(PHONE_NUMBER, null);
        firstName = preferences.getString(FIRST_NAME, null);
        lastName = preferences.getString(LAST_NAME, null);

        if (phoneNumber == null) {
            return null;
        }

        if (firstName == null) {
            return null;
        }

        if (lastName == null) {
            return null;
        }


        if(preferences.getBoolean("developer", false)) {
            Log.d("Identity", "PhoneNumber: " + phoneNumber + ", Firstname: " + firstName + ", Lastname: " + lastName);
        }

        return new Identity(phoneNumber, firstName, lastName);
    }

    public static void saveToPreferences(Identity identity, Context context) {
        SharedPreferences preferences = context.getSharedPreferences(
                context.getPackageName(), Context.MODE_PRIVATE
        );
        SharedPreferences.Editor editor = preferences.edit();

        editor.putString(PHONE_NUMBER, identity.getPhoneNumber());
        editor.putString(FIRST_NAME, identity.getFirstName());
        editor.putString(LAST_NAME, identity.getLastName());
        editor.apply();
    }
}
