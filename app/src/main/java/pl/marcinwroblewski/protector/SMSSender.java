package pl.marcinwroblewski.protector;

import android.telephony.SmsManager;

import java.util.ArrayList;

/**
 * Created by Marcin Wróblewski on 16.01.2017.
 */

public class SMSSender {

    public static OperationResult sendSMSTo(String phoneNumber, String message) {

        SmsManager smsManager = SmsManager.getDefault();
        ArrayList<String> msgStringArray = smsManager.divideMessage(message);
        smsManager.sendMultipartTextMessage(phoneNumber, null, msgStringArray, null, null);

        return new OperationResult(OperationResult.STATUS_SUCCESS, "SMS sent successfully");
    }

}
