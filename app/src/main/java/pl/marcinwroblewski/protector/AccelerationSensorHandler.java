package pl.marcinwroblewski.protector;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.SensorEvent;

/**
 * Created by Marcin Wróblewski on 19.05.2016.
 */
public class AccelerationSensorHandler {

    private int sensivity;
    private SharedPreferences sp;

    public static String turnValuesIntoString(SensorEvent event) {
        String values = "\n";
        for(float value : event.values)
            values += (int)value + "\n";

        return values;
    }

    public AccelerationSensorHandler(Context context) {
        sp = context.getSharedPreferences(context.getPackageName(), Context.MODE_PRIVATE);
        sensivity = sp.getInt("sensivity", 20);
    }

    public boolean detectDanger(float[] data) {
        for(float value : data) {
            if (value > sensivity || value < -(sensivity)) {
                return true;
            }
        }

        return false;
    }

}
