package pl.marcinwroblewski.protector;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.provider.Settings;
import android.text.TextUtils;
import android.view.View;

import com.geniusforapp.fancydialog.FancyAlertDialog;

/**
 * Created by Marcin Wróblewski on 31.03.2017.
 */

public class LocationServicesCheck {

    public static boolean isLocationEnabled(Context context) {
        int locationMode = 0;
        String locationProviders;

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT){
            try {
                locationMode = Settings.Secure.getInt(context.getContentResolver(), Settings.Secure.LOCATION_MODE);

            } catch (Settings.SettingNotFoundException e) {
                e.printStackTrace();
                return false;
            }

            return locationMode == Settings.Secure.LOCATION_MODE_HIGH_ACCURACY;

        } else {
            locationProviders = Settings.Secure.getString(context.getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);
            return !TextUtils.isEmpty(locationProviders);
        }
    }

    public static void showLocationWarning(final Activity activity) {
        FancyAlertDialog.Builder alert = new FancyAlertDialog.Builder(activity)
                .setImageDrawable(activity.getResources().getDrawable(R.drawable.ic_critical_warning))
                .setTextTitle("Warning")
                .setTextSubTitle("Can't locate you in case of emergency")
                .setBody("Your location services has been set up in a way that prevent Protector from obtaining your location. That's very dangerous.\nPlease go to settings and set a location services to a highest accuracy.")
                .setNegativeColor(R.color.background)
                .setNegativeButtonText("Later")
                .setOnNegativeClicked(new FancyAlertDialog.OnNegativeClicked() {
                    @Override
                    public void OnClick(View view, Dialog dialog) {
                        dialog.dismiss();
                    }
                })
                .setPositiveButtonText("Open settings")
                .setPositiveColor(R.color.colorAccent)
                .setOnPositiveClicked(new FancyAlertDialog.OnPositiveClicked() {
                    @Override
                    public void OnClick(View view, Dialog dialog) {
                        Intent viewIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        activity.startActivity(viewIntent);
                        dialog.dismiss();
                    }
                })
                .setAutoHide(false)
                .build();
        alert.show();
    }

}
