package pl.marcinwroblewski.protector;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;
import android.test.ApplicationTestCase;
import android.test.RenamingDelegatingContext;
import android.util.Log;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;


/**
 * Created by Marcin Wróblewski on 11.01.17.
 */
@RunWith(AndroidJUnit4.class)
@LargeTest
public class DangerDetectionHelper_OnlyAccelerometer_Test extends ApplicationTestCase<ProtectorApplication> {

    private Context context;

    public DangerDetectionHelper_OnlyAccelerometer_Test() {
        super(ProtectorApplication.class);
    }

    @Before
    public void setUp() {
        context = new RenamingDelegatingContext(InstrumentationRegistry.getTargetContext(), "test_");
    }

    @Ignore
    private Context getTestContext() {
        return context;
    }

    @Test
    public void DangerDetection_DetectDanger_ReturnsTrue() {
        float difference = 20; //default dangerous level

        DangerDetectionHelper helper = new DangerDetectionHelper(getTestContext(),
                DangerDetectionHelper.MODE_ACCELEROMETER);

        Assert.assertTrue(helper.detectDanger(difference));
    }

    @Test
    public void DangerDetection_DetectDanger_ReturnsFalse() {
        float difference = 5;

        DangerDetectionHelper helper = new DangerDetectionHelper(getTestContext(),
                DangerDetectionHelper.MODE_ACCELEROMETER);

        if(!helper.detectDanger(difference)) {
            System.out.print("Test failed! Check app settings (\"maxDangerPoints\")");
        }

        Assert.assertFalse(helper.detectDanger(difference));
    }

    @Test
    public void DangerDetection_TurnOnDangerDetection() {
        Context context = getTestContext();

        DangerDetectionHelper helper = new DangerDetectionHelper(context,
                DangerDetectionHelper.MODE_ACCELEROMETER);
        OperationResult result = helper.turnOnDangerDetection();

        Log.d("TurnOnDangerDetection", result.toString());

        Assert.assertEquals(OperationResult.STATUS_SUCCESS, result.getStatus());
    }

    @Test
    public void DangerDetection_TurnOffDangerDetection() {
        Context context = getTestContext();

        DangerDetectionHelper helper = new DangerDetectionHelper(context,
                DangerDetectionHelper.MODE_ACCELEROMETER);
        OperationResult result = helper.turnOffDangerDetection();

        Log.d("TurnOffDangerDetection", result.toString());

        Assert.assertEquals(OperationResult.STATUS_SUCCESS, result.getStatus());
    }

    @Test
    public void DangerDetection_StartCollectingData() {
        Context context = getTestContext();

        DangerDetectionHelper helper = new DangerDetectionHelper(context,
                DangerDetectionHelper.MODE_ACCELEROMETER);
        OperationResult result = helper.startCollectingData();

        Log.d("StartCollectingData", result.toString());

        Assert.assertEquals(OperationResult.STATUS_SUCCESS, result.getStatus());
    }

    @Test
    public void DangerDetection_EndCollectingData() {
        Context context = getTestContext();

        DangerDetectionHelper helper = new DangerDetectionHelper(context,
                DangerDetectionHelper.MODE_ACCELEROMETER);
        OperationResult result = helper.endCollectingData();

        Log.d("EndCollectingData", result.toString());

        Assert.assertEquals(OperationResult.STATUS_SUCCESS, result.getStatus());
    }
}
