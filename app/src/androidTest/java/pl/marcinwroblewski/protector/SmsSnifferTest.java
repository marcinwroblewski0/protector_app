package pl.marcinwroblewski.protector;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.LargeTest;
import android.support.test.runner.AndroidJUnit4;
import android.test.ApplicationTestCase;
import android.test.RenamingDelegatingContext;

import org.json.JSONException;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

import pl.marcinwroblewski.protector.guardian.Guardian;

/**
 * Created by Admin on 28.01.2017.
 */

@RunWith(AndroidJUnit4.class)
@LargeTest
public class SmsSnifferTest extends ApplicationTestCase<ProtectorApplication> {

    private Context context;

    public SmsSnifferTest() {
        super(ProtectorApplication.class);
    }

    @Before
    public void setUp() {
        context = new RenamingDelegatingContext(InstrumentationRegistry.getTargetContext(), "test_");
    }

    @Ignore
    private Context getTestContext() {
        return context;
    }

    @Test
    public void allConfigurationSms() {
        ConfigurationSmsSniffer smsSniffer = new ConfigurationSmsSniffer(getTestContext());
        smsSniffer.getAllConfigurationMessages();
    }

    @Test
    public void extractConversationForNumber() {
        ConfigurationSmsSniffer smsSniffer = new ConfigurationSmsSniffer(getTestContext());
        smsSniffer.extractConversation("48790756648");
    }

    @Test
    public void findAskingGuardians() throws JSONException {
        ConfigurationSmsSniffer smsSniffer = new ConfigurationSmsSniffer(getTestContext());
        for (Guardian guardian : smsSniffer.findRequestingGuardians()) {
            System.out.println(guardian.toJSONObject().toString(2));
        }
    }

    @Test
    public void restoreRequestsStatusFromInbox() throws JSONException {
        ConfigurationSmsSniffer smsSniffer = new ConfigurationSmsSniffer(getTestContext());
        for (Guardian guardian : smsSniffer.findRequestingGuardians()) {
            smsSniffer.restoreRequestStatus(
                    smsSniffer.extractConversation(guardian.getPhoneNumber()),
                    guardian);
        }
    }

}
